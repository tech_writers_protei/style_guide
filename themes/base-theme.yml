extends: default

header:
  height: 1.5cm
  image:
    vertical:
      align: 11
  padding:
    top: 0.7
  border:
    color: #B5B5B5
    width: 0.1
  font:
    size: 0.6cm
    color: #6f6f6f
  recto:
    right:
      content: image::../images/logo.png[pdfwidth=25vw]
    left:
      content: '{document-title}'
  verso:
    left:
      content: $header-recto-right-content
    right:
      content: $header-recto-left-content

footer:
  recto:
    right:
      content: '{page-number} из {page-count}'
    left:
      content: '{docdate}'
    center:
      content: '{section-or-chapter-title}'
  verso:
    left:
      content: '{page-number} из {page-count}'
    right:
      content: '{docdate}'
    center:
      content: '{section-or-chapter-title}'

admonition:
  font:
    family: $base-font-family
    size: $base-font-size
  label:
    font:
      color: #CCCCCC
      size: $base-font-size

image:
  align: center
  caption:
    align: center
    end: bottom
    text:
      align: center
  border:
    color: #000
    fit: content
    style: solid
    width: 1

page:
  layout: portrait
  margin: [20mm, 10mm, 10mm, 25mm]
  size: A4
  numbering:
    start:
      at: after-toc

base:
  font:
    family: PT Sans Web
    size: 10
    color: #000
    kerning: normal
    style: normal
  text:
    align: left

caption:
  font:
    color: $base-font-color
    family: $base-font-family
    size: $base-font-size
    style: $base-font-style
  text:
    align: left

code:
  background:
    color: #fff
  border:
    color: #fff
  font:
    color: $base-font-color
    family: PT Mono
    size: $base-font-size
    style: normal
  padding: [0.5, 5]
  caption:
    font:
      size: $base-font-size

codespan:
  background:
    color: #fff
  font:
    family: $code-font-family
    size: $code-font-size
    style: $code-font-style
    color: $code-font-color

description-list:
  description:
    indent: 15
  term:
    font:
      family: $base-font-family
      size: $base-font-size
      kerning: $base-font-kerning
      color: #333

heading:
  font:
    color: $base-font-color
    family: $base-font-family
    style: bold
    kerning: normal
  text:
    align: left
    transform: None
  min:
    height:
      after: auto
  part:
    break:
      before: always
      after: avoid

kbd:
  font:
    color: $base-font-color
    family: $code-font-family
    size: $base-font-size
    style: $base-font-style

link:
  font:
    color: #000066
    family: $base-font-family
    style: $base-font-style
  text:
    decoration: underline

list:
  indent: 15
  item:
    spacing: 5
  marker:
    font:
      color: $base-font-color
  text:
    align: left

running-content:
  start:
    at: after-toc

svg:
  fallback:
    font:
      family: Roboto

table:
  align: left
  stripes: none
  background:
    color: #fff
  caption:
    align: left
    text:
      align: left
      size: $caption-font-size
    end: top
  border:
    color: #000
    style: solid
    width: 0.5
  cell:
    padding: 2
  font:
    family: $base-font-family
    color: #000
    kerning: $base-font-kerning
    size: 0.8rem
    style: $base-font-style
  width: auto
  head:
    border:
      bottom:
        width: 1
    font:
      style: bold
    text:
      transform: none

title-page:
  logo:
    align: center
    image: ../images/logo.png

toc:
  break-after: auto
  dot-leader:
    levels: none

font:
  catalog:
    merge: true
    Roboto:
      normal: roboto-normal.ttf
      italic: roboto-italic.ttf
      bold: roboto-bold.ttf
      bold_italic: roboto-bold_italic.ttf
    PT Serif Web:
      normal: pt-serif-web-normal.ttf
      italic: pt-serif-web-italic.ttf
      bold: pt-serif-web-bold.ttf
      bold_italic: pt-serif-web-bold_italic.ttf
    PT Sans Web:
      normal: pt-sans-web-normal.ttf
      italic: pt-sans-web-italic.ttf
      bold: pt-sans-web-bold.ttf
      bold_italic: pt-sans-web-bold_italic.ttf
    PT Mono: pt-mono.ttf
    Droid Sans Fallback: droid-sans-fallback.ttf
    Times New Roman: times-new-roman-normal.ttf
  fallbacks:
    - Droid Sans Fallback
    - Times New Roman
    - Roboto
