[[using-draw-io]]
= Основы работы с draw.io
:revdate: Jun 1, 2023
:revnumber: 1.0.0
:asciidoctorconfigdir: ../

ifndef::imagesdir[:imagesdir: ../images]

[[introduction-draw-io]]
== Краткое знакомство

// tag::introduction-draw-io[]
`draw.io`, а точнее https://app.diagrams.net/[app.diagrams.net] --графический редактор для создания схем, диаграмм, чертежей и т.п.

* Распространяется свободно.
* Имеет значительный набор иконок, базовых элементов схем и нотаций из коробки, что упрощает и ускоряет работу с графикой.
* Имеет встроенный конвертер для преобразования в форматы PNG, JPEG, VSDX, SVG, XML.
* Позволяет хранить файлы как в облачном хранилище, так и локально.

== Создание нового файла

. Перейти на https://app.diagrams.net/.
. Выбрать место для хранения файла:
* https://drive.google.com/[Google Drive];
* https://onedrive.live.com/[OneDrive];
* https://www.dropbox.com/ru/[Dropbox];
* https://github.com/[GitHub];
* https://gitlab.com/[GitLab];
* локально;

image::draw_io_select_storage.png[pdfwidth=80%,scaledwidth=80%]

NOTE: Можно пропустить этот пункт и сделать выбор позднее.

[start=2]
. В углу задать название файла, изменив имя по умолчанию `'Untitled Diagram'`.
. На нижней панели задать название странице, изменив имя по умолчанию `'Page-1'`.
. На боковой панели загрузить иконки в раздел `'Scratchpad'`.

image::draw_io_main_page.png[width=640,height=309,title="Title"]

* Вариант 1 --- перетащить xml-файл в область раздела.
* Вариант 2:
** Нажать кнопку btn:[Edit].
** Вариант 2.1 --перетащить xml-файл в указанную область.
** Вариант 2.2 --нажать кнопку btn:[Import] и указать xml-файл.

image::draw_io_upload.png[width=351,height=251]

== Корректное сохранение файла

Для корректного отображения сохраненного изображения нельзя использовать конвертацию файла в SVG напрямую.

.Почему?
[%collapsible]
====

. В этом случае не загружаются использованные шрифты.

В *draw.io* используется Google Fonts API для загрузки шрифтов.
Генератор статических сайтов Hugo не поддерживает svg-файлы со встроенными "скриптами", в частности, `@include`.

[start=2]
. В этом случае не подгружается часть форм.

В *draw.io* пользовательские фигуры хранятся как векторные изображения, которые хранятся как отдельные файлы.
Генератор статических сайтов Hugo не поддерживает загрузку сторонних файлов внутри svg-файлов.
====

. Экспортировать файл как SVG, *menu:File[Export as > SVG...]*.

image::draw_io_export_as.png[width=360,height=208]

[start=2]
. В поле `Text Settings` выбрать вариант `Convert labels to SVG`.

image::../images/draw_io_convert_labels.png[width=201,height=248]

[start=3]
. Сохранить файл.
. Добавить файл в проект.
// end::introduction-draw-io[]