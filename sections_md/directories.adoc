[[project-directories]]
= Директории проекта
:asciidoctorconfigdir: ..

[horizontal]
/usr/protei/%ProjectDir%/archive/:: директория для архивных файлов журналов
/usr/protei/backup/%ProjectDir%/:: директория для резервных копий с других узлов
/usr/protei/%ProjectDir%/bin/:: директория для исполняемых файлов
/usr/protei/%ProjectDir%/bin/utils/:: директория для запускаемых скриптов, реализующих основной функционал скриптовой оболочки
/usr/protei/%ProjectDir%/cdr/:: директория для журналов CDR
/usr/protei/%ProjectDir%/conf/:: директория для общих файлов настройки сервера Tomcat
/usr/protei/%ProjectDir%/config/:: директория для конфигурационных файлов
/usr/protei/%ProjectDir%/db/:: директория для конфигурации параметров базы данных
/usr/protei/%ProjectDir%/history/:: директория для архивных лог-файлов
/usr/protei/%ProjectDir%/jar/:: директория для архивированных файлов с классами Java
/usr/protei/%ProjectDir%/lib/:: директория для сторонних подключаемых библиотек
/usr/protei/%ProjectDir%/logs/:: директория для журналов
/usr/protei/%ProjectDir%/metrics/:: директория для файлов метрик
/usr/protei/%ProjectDir%/scripts/:: директория для хранения скриптов CLI
/usr/protei/%ProjectDir%/stat/:: директория для файлов статистики
/usr/protei/%ProjectDir%/temp/:: директория для временных файлов
/usr/protei/%ProjectDir%/webapps/:: директория для файлов Web-интерфейса