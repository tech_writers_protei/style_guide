[[cmd-unix]]
= Командная строка *nix
:revnumber: 1.0.0
:asciidoctorconfigdir: ..

== Базовые команды

=== Отобразить путь до текущей директории, pwd

[source,bash]
----
$ pwd
----

.Пример
[source,console]
----
$ pwd
/usr/protei
----

=== Перейти в другую директорию, cd

[source,bash]
----
$ cd <path>
----

.Пример
[source,console]
----
$ cd protei
$ pwd
/usr/protei
----

* */* --- перейти в корень;
* *~* --- перейти в домашнюю директорию;
* *..* --- перейти в родительскую директорию

.Пример
[source,console]
----
$ cd /
[support@Server /] $ cd ~
[support@Server ~] $ cd ..
----

=== Отобразить содержимое директории, ls

[source,bash]
----
$ ls
----

.Пример
[source,console]
----
$ ls
X11    bin  libexec  sbin   standalone
X11R6  lib  local    share
----

=== Отобразить содержимое директории, включая скрытые файлы, ls -a

[source,bash]
----
$ ls -a
----

.Пример
[source,console]
----
$ ls -a
.    X11R6  libexec  share
..   bin    local    standalone
X11  lib    sbin
----

=== Отобразить подробную информацию о содержимом директории, ls -l

[source,bash]
----
$ ls -l
----

.Пример
[source,console]
----
$ ls -l
total 0
-rw-r--r--   1 root           wheel     0 Jan  1  2020 .localized
drwxrwxrwt   9 root           wheel   288 Jan  1  2020 Shared
drwxr-xr-x+ 42 AndrewTarasov  staff  1344 May  2 21:44 user
----

=== Вывести помощь для команды на экран, man

[source,bash]
----
$ man <command>
----

.Пример
[source,console]
----
$ man mkdir
MKDIR(1)                         User Commands                        MKDIR(1)

NAME
       mkdir --make directories

SYNOPSIS
       mkdir [OPTION]... DIRECTORY...

DESCRIPTION
       Create the DIRECTORY(ies), if they do not already exist.

       Mandatory  arguments  to  long  options are mandatory for short options too.

       -m, --mode=MODE
              set file mode (as in chmod), not a=rwx --umask

       -p, --parents
              no error if existing, make parent directories as needed

       -v, --verbose
              print a message for each created directory

 Manual page mkdir(1) line 1 (press h for help or q to quit)
----

=== Повторить последнюю команду, !!

[source,bash]
----
$ !!
----

.Пример
[source,console]
----
$ cd ..
$ !!
cd ..
----

=== Выполнить команду от имени суперпользователя, sudo

[source,bash]
----
$ sudo <command>
----

.Пример
[source,console]
----
$ sudo cd root
----

NOTE: При использовании *sudo* на запрос пароля от системы необходимо ввести пароль от учетной записи *текущего* пользователя.


[#su]
=== Сменить текущего пользователя на суперпользователя, su

[source,bash]
----
$ su
----

.Пример
[source,console]
----
$ su
Password: <password>
----

NOTE: При использовании *su* на запрос пароля от системы необходимо ввести пароль от учетной записи *суперпользователя*.

=== Выйти из любой оболочки, exit

[source,bash]
----
$ exit
----

.Пример
[source,console]
----
$ exit
----

=== Переместить строку с курсором на самый верх, clear

[source,bash]
----
$ clear
----

.Пример
[source,bash]
----
$ clear
----

=== Отобразить справочную информацию для команды, <command> --help

[source,bash]
----
$ <command> -h
<command> --help
----

.Пример
[source,console]
----
$ man --help
man, version 1.6g

usage: man [-adfhktwW] [section] [-M path] [-P pager] [-S list]
	[-m system] [-p string] name ...

  a : find all matching entries
  c : do not use cat file
  d : print gobs of debugging information
  D : as for -d, but also display the pages
  f : same as whatis(1)
  h : print this help message
  k : same as apropos(1)
  K : search for a string in all pages
  t : use troff to format pages for printing
  w : print location of man page(s) that would be displayed
      (if no name given: print directories that would be searched)
  W : as for -w, but display filenames only

  C file   : use `file' as configuration file
  M path   : set search path for manual pages to `path'
  P pager  : use program `pager' to display pages
  S list   : colon separated section list
  m system : search for alternate system's man pages
  p string : string tells which preprocessors to run
               e --[n]eqn(1)   p --pic(1)    t --tbl(1)
               g --grap(1)     r --refer(1)  v --vgrind(1)
----

=== Отобразить содержимое файлов на одном экране вместе, cat

[source,bash]
----
$ cat <file_name> ... <file_name>
----

.Пример
[source,console]
----
$ cat test_1.txt test_2.txt
Text 1 Line 1
Text 1 Line N
Text 2 Line 1
Text 2 Line M
----

=== Отобразить содержимое текстового файла, less

[source,bash]
----
$ less <file_name>
----

.Пример
[source,console]
----
$ less test.txt
Text 1 Line 1
Text 1 Line N
----

=== Запустить скрипт, sh

[source,bash]
----
$ sh <path_script>
----

.Пример
[source,bash]
----
$ sh ./product/version
----

== Дополнительные команды

NOTE: Эти команды будут использоваться на порядок реже, чем указанные выше, тем не менее, также полезны в некоторых случаях и для некоторых задач.

=== Создать директорию, mkdir

[source,bash]
----
$ mkdir <dir_name>
----

.Пример
[source,console]
----
$ mkdir test
----

=== Создать директорию, создавая не существующие папки по пути, mkdir -p

[source,bash]
----
$ mkdir -p <dir_name>
----

.Пример
[source,console]
----
$ mkdir -p test/test
----

=== Переместить файл, mv

[source,bash]
----
$ mv <file_name> <new_path>
----

.Пример
[source,bash]
----
$ mv test.txt ./test_dir
----

=== Переименовать файл, mv

[source,bash]
----
$ mv <old_file_name> <new_file_name>
----

.Пример
[source,bash]
----
$ mv test.txt new.txt
----

===  Копировать файл, cp

[source,bash]
----
$ cp <file_name> <new_path>
----

.Пример
[source,bash]
----
$ cp test.txt ./test_dir
----

=== Удалить файл, rm

[source,bash]
----
$ rm <file_name>
----

.Пример
[source,bash]
----
$ rm test.txt
----

=== Удалить файл с предварительным подтверждением, rm -i

[source,bash]
----
$ rm -i <file>
----

.Пример
[source,console]
----
$ rm -i test.txt
remove test.txt? y
----

=== Удалить пустую директорию

[source,bash]
----
$ rmdir <dir_name>
----

.Пример
[source,console]
----
$ rmdir test
----

=== Принудительно удалить директорию рекурсивно, rm -rf

[source,bash]
----
$ rm -rf <dir_name>
----

.Пример
[source,bash]
----
$ rm -rf test
----

=== Показать все процессы пользователя, ps

[source,bash]
----
$ ps
----

.Пример
[source,console]
----
$ ps
PID  TTY        TIME    CMD
2468 ttys000    0:00.22 -zsh
8390 ttys000    0:00.15 /Library/Developer/CommandLineTools/usr/bin/git -C /us
8391 ttys000    0:14.15 /Library/Developer/CommandLineTools/usr/libexec/git-co
8452 ttys000    0:28.52 /Library/Developer/CommandLineTools/usr/libexec/git-co
8407 ttys001    0:00.14 -zsh
----

=== Принудительно завершить процесс, kill/kill -15

[source,bash]
----
$ kill <process_id>
$ kill -15 <process_id>
----

.Пример
[source,bash]
----
$ kill 8525
$ kill -15 8526 8527
----

=== Немедленно принудительно завершить процесс, kill -9

[source,bash]
----
$ kill -9 <process_id>
----

.Пример
[source,bash]
----
$ kill -9 8525
----

=== Открыть минималистичный GUI-текстовый редактор в командной строке, nano

[source,bash]
----
$ nano <file_name>
----

.Пример
[source,console]
----
$ nano
GNU nano 2.0.6                New Buffer

^G Get Help  ^O WriteOut  ^R Read File ^Y Prev Page ^K Cut Text  ^C Cur Pos
^X Exit      ^J Justify   ^W Where Is  ^V Next Page ^U UnCut Text^T To Spell
----

=== Вывод информации о текущих процессах и состоянии системы, top

[source,bash]
----
$ top
----

.Пример
[source,console]
----
$ top
Processes: 264 total, 7 running, 4 stuck, 253 sleeping, 1883 threads   22:24:02
Load Avg: 2.93, 3.28, 3.74  CPU usage: 57.7% user, 10.27% sys, 32.64% idle
SharedLibs: 199M resident, 51M data, 14M linkedit.
MemRegions: 317181 total, 4194M resident, 35M private, 364M shared.
PhysMem: 8042M used (2667M wired), 149M unused.
VM: 1425G vsize, 2305M framework vsize, 537950136(4960) swapins, 542685115(16345
Networks: packets: 105734371/137G in, 29520336/3566M out.
Disks: 126839932/2682G read, 42074223/2487G written.

PID    COMMAND      %CPU  TIME     #TH   #WQ  #PORTS  MEM    PURG  CMPRS  PGRP
1513   Atom Helper  209.9 05:27:26 23/4  1    192     3249M- 0B    236M-  1468
0      kernel_task  17.0  31:26:32 176/4 0    0       612M-  0B    0B     0
135    WindowServer 11.8  125 hrs  13    6    2913-   349M+  152K- 115M-  135
2461   Terminal     9.2   01:47.76 9     3    502-    34M-   868K+ 17M-   2461
10076  top          7.9   00:01.38 1/1   0    26      7528K  0B    0B     10076
9761   gamecontroll 1.3   00:09.88 5/2   4/1  67      1460K+ 0B    532K   9761
2408   plugin-conta 0.8   06:08:46 30    1    3399    787M   0B    741M-  2391
1468   Atom         0.8   06:24:02 34    2    416     116M+  0B    59M    1468
1472   ControlCente 0.7   09:33.92 5     2    560+    39M-   0B    28M-   1472
1588   firefox      0.5   87:15:06 93    5    131476+ 1795M+ 0B    1586M- 1588
74     powerd       0.5   10:27.91 4     3    127+    2540K+ 0B    992K-  74
2391   Ghostery     0.3   51:55:36 86    5    754+    1027M+ 0B    853M-  2391
2404   plugin-conta 0.3   04:12:24 33    1    1324    377M   0B    362M   2391
2400   plugin-conta 0.3   05:08:39 30    1    2189    999M   0B    937M-  2391
61     UserEventAge 0.3   11:24.74 6     3    701+    2972K+ 0B    1596K- 61
522    corespotligh 0.3   00:39.11 7     5    131+    6404K+ 0B    5500K- 522
----

=== Найти файл по сохраненной базе, locate

IMPORTANT: Выполняется быстро, но не отображает все последние изменения.

[source,bash]
----
$ locate <file_name>
----

.Пример
[source,console]
----
$ locate temp
/usr/protei/temp
----

=== Найти файл по сохраненной базе без учета регистра, locate -i

IMPORTANT: Выполняется быстро, но не отображает все последние изменения.

[source,bash]
----
$ locate -i <file_name>
----

.Пример
[source,console]
----
$ locate -i temp
/usr/protei/Template.txt
----

=== Найти файл, find

[source,bash]
----
$ find <path> -name "<file_name>"
----

.Пример
[source,console]
----
$ find /usr/protei -iname "temp*"
/usr/protei/Template.txt
/usr/protei/test/temp
----

=== Найти строки по содержимому с помощью регулярных выражений, grep

[source,bash]
----
$ grep '<pattern>' <file_name>
----

.Пример
[source,console]
----
$ grep "Line *" /usr/protei/test.txt
----

* -i/--ignore-case --- искать без учета регистра;
* -H --- выводить название файла вместе с найденными строками;
* -h/--no-filename --- не выводить названия файлов, где найдены подходящие строки;
* -a/--text --- рассматривать все файлы как ASCII, в том числе и бинарные;
* -n/--line-number --- указать номер строки, где встречается искомое выражение, для каждого файла, начиная с 1, строки каждого файла нумеруются отдельно;
* --null --- напечатать \0 после названия файла;
* -R/-r/--recursive --- искать рекурсивно по всем поддиректориям;
* -v/--invert-match --- вывести строки, в которых отсутствует указанное выражение;

=== Распаковать архив *.tar, tar -xvf

[source,bash]
----
$ tar -xvf <archive_name>
----

.Пример
[source,console]
----
$ tar -xvf test.tar
----

=== Отобразить настройки сетевых интерфейсов, ifconfig

[source,bash]
----
$ ifconfig
----

.Пример
[source,console]
----
$ ifconfig
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> mtu 16384
	options=1203<RXCSUM,TXCSUM,TXSTATUS,SW_TIMESTAMP>
	inet 127.0.0.1 netmask 0xff000000
	inet6 ::1 prefixlen 128
	inet6 fe80::1%lo0 prefixlen 64 scopeid 0x1
	nd6 options=201<PERFORMNUD,DAD>
gif0: flags=8010<POINTOPOINT,MULTICAST> mtu 1280
stf0: flags=0<> mtu 1280
XHC20: flags=0<> mtu 0
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether 30:35:ad:db:d2:60
	inet 192.168.0.10 netmask 0xffffff00 broadcast 192.168.0.255
	media: autoselect
	status: active
en1: flags=8963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
	options=460<TSO4,TSO6,CHANNEL_IO>
	ether 82:18:46:04:f4:00
	media: autoselect <full-duplex>
	status: inactive
bridge0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=63<RXCSUM,TXCSUM,TSO4,TSO6>
	ether 82:18:46:04:f4:00
	Configuration:
		id 0:0:0:0:0:0 priority 0 hellotime 0 fwddelay 0
		maxage 0 holdcnt 0 proto stp maxaddr 100 timeout 1200
		root id 0:0:0:0:0:0 priority 0 ifcost 0 port 0
		ipfilter disabled flags 0x0
	member: en1 flags=3<LEARNING,DISCOVER>
	        ifmaxaddr 0 port 6 priority 0 path cost 0
	media: <unknown type>
	status: inactive
p2p0: flags=8843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST> mtu 2304
	options=400<CHANNEL_IO>
	ether 02:35:ad:db:d2:60
	media: autoselect
	status: inactive
awdl0: flags=8943<UP,BROADCAST,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1484
	options=400<CHANNEL_IO>
	ether e2:4f:45:97:aa:d5
	inet6 fe80::e04f:45ff:fe97:aad5%awdl0 prefixlen 64 scopeid 0x9
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
llw0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=400<CHANNEL_IO>
	ether e2:4f:45:97:aa:d5
	inet6 fe80::e04f:45ff:fe97:aad5%llw0 prefixlen 64 scopeid 0xa
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
utun0: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1380
	inet6 fe80::fe74:4102:cf53:76ad%utun0 prefixlen 64 scopeid 0xb
	nd6 options=201<PERFORMNUD,DAD>
utun1: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 2000
	inet6 fe80::c9f4:2f34:b38c:4dd6%utun1 prefixlen 64 scopeid 0xc
	nd6 options=201<PERFORMNUD,DAD>
utun2: flags=8051<UP,POINTOPOINT,RUNNING,MULTICAST> mtu 1000
	inet6 fe80::ce81:b1c:bd2c:69e%utun2 prefixlen 64 scopeid 0xd
	nd6 options=201<PERFORMNUD,DAD>
----

== Некоторые сложные полезные команды

=== Вывести на экран все конфигурационные файлы

.Вариант 1
[source,shell]
----
while read -r line do
  echo "$line" && cat $line && echo $'\n' done
<<< $(cd /usr/protei/<product>/config/ && ls -a | grep -E '\.cfg|\.conf');
----

.Вариант 2
[source,shell]
----
cd /path/to/directory/;
VAR="cat `ls -a | grep -E '\.cfg|\.conf'`";
eval $VAR;
----

=== Вывести на экран все конфигурационные файлы, кроме набора исключений

.Вариант 1
[source,shell]
----
while read -r line; do
  echo "$line" && cat $line && echo $'\n'; done
<<< $(cd /usr/protei/<product>/config/ && ls -a | grep -E '\.cfg|\.conf' | grep -Ev '<file_name_1>|<file_name_2>');
----

.Вариант 2
[source,shell]
----
cd /path/to/directory/;
VAR="cat `ls -a | grep -E '\.cfg|\.conf' | grep -Ev '<file_name_1>|<file_name_2>'`";
eval $VAR;
----
