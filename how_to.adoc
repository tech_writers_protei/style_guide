= Руководство HOW TO
:author: Andrew Tar
:appendix-caption: Приложение
:caution-caption: Внимание
:copyright: Без предварительного письменного разрешения, полученного от ООО "НТЦ ПРОТЕЙ", \
настоящий документ и любые выдержки из него, с изменениями и переводом на другие языки, \
не могут быть воспроизведены или использованы.
:docname: How-To
:description: Практическое руководство по решению часто встречающихся задач при разработке документации.
:email: tarasov-a@protei.ru
:revdate: Mar 1, 2024
:appendix-caption: Приложение
:appendix-refsig: Приложение
:author: Andrew Tar
:caution: Предупреждение
:chapter-signifier:
:email: andrew.tar@yahoo.com
:example-caption: Пример
:experimental:
:figure-caption: Рисунок
:icons: font
:important-caption: Важно
:noheader:
:nofooter:
:note-caption: Примечание
:pagenums:
:partnums:
:pdf-fontsdir: fonts,GEM_FONTS_DIR
:pdf-page-layout: portrait
:pdf-theme: base-theme.yml
:pdf-themesdir: themes
:revnumber: 1.1.2
:sectids:
:section-refsig:
:sectlinks:
:sectnumlevels: 5
:sectnums:
:showtitle:
:source-highlighter: rouge
:stylesdir: css
:stylesheet: default.css
:table-caption: Таблица
:text-align: left
:tip-caption: Совет
:title-page:
:toc-title: Содержание
:toc: auto
:toclevels: 5
:warning-caption: Предупреждение
:xrefstyle: full
:kroki-fetch-diagram:

:imagesdir: images

В документе приведены практические советы и примеры для решения некоторых возможных сложностей при создании документации, не привлекая внимание санитаров.

:leveloffset: +1

include::how_to/introduction.adoc[]

include::how_to/creating_project.adoc[]

include::how_to/adding_links_md.adoc[]

include::how_to/adding_links_adoc.adoc[]

include::how_to/generating_yaml_for_pdf.adoc[]

include::how_to/fixing_pipeline.adoc[]

include::how_to/filling_placeholders.adoc[]

include::how_to/hiding_files.adoc[]

include::how_to/modifying_styles.adoc[]

include::how_to/generating_html_pdf.adoc[]

include::how_to/determining_icon.adoc[]

include::how_to/exporting_images.adoc[]

include::how_to/adding_proofreading.adoc[]

include::how_to/installing_jetbrains_plugin.adoc[]

include::how_to/tips_and_tricks.adoc[]

include::how_to/ascii_doc_basics.adoc[]

:leveloffset: -1