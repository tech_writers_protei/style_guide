[[huawei-documentation]]
= Работа с документацией Huawei
:author: Andrew Tar
:email: andrew.tar@yahoo.com
:revdate: Jun 14, 2023
:revnumber: 1.0.0
:experimental:
:icons: font
// :asciidoctorconfigdir: ../

ifndef::imagesdir[:imagesdir: ../images]

== Краткая сводка

В наличии имеется ряд документов, созданных компанией Huawei для своих разработок. Предполагается, что документация для Tele2 должна равняться на международный уровень, поэтому ее возможно использовать в качестве примера окончательного результата. Т.е. как наглядное пособие, на что должны быть примерно похожи итоги наших трудов.

== Просмотр документации Huawei

Для просмотра файлов требуются:

* Собственно, сами файлы документов;
* Программное обеспечение для просмотра.

Файлы документов представлены в формате *.HDX и недоступны для использования без специального ПО. Файлы и ПО выложены на https://cloud.protei.ru/s/r8q4J2DOhy9yHj9[корпоративном облаке Cloud Protei].

== Работа с файлами

Для полноценной работы с файлами документации необходимо выполнить следующие действия:

. Выгрузить файлы документации из директории https://cloud.protei.ru/s/r8q4J2DOhy9yHj9[Tele2 EPC] на корпоративном облаке.
+
TIP: https://cloud.protei.ru/apps/files/ajax/download.php?dir=/Tele2%20EPC&files=%5B%22CloudePDG_V100R019C10_02_en_CEH1026H.hdx%22,%22CloudUGW_V100R019C10_04_en_CEI0516G.hdx%22,%22CloudUGW_V100R019C10_06_en_CEI0126T.hdx%22,%22CloudUGW_V100R020C50_02_en_CEJ0604G.hdx%22,%22CloudUGW_V100R020C50_03_en_CEJ0605S.hdx%22,%22CloudUSN_V100R019C10_02_en_CEI0520D.hdx%22,%22CloudUSN_V100R020C50_01_en_CEJ0610J.hdx%22,%22Huawei%20AMF_SMF_NRF_NSSF%20UNC_21.3.0_01_en_CEK0806G.hdx%22,%22Huawei%20EPC%20UGW9811_V900R018C10_07.hdx%22,%22Huawei%20SGSN_MME%20USN9810_V900R019C60SPC200_01_en_CEI1118T.hdx%22,%22Huawei%20UPF_SGWU_PGWU%20UDG_21.3.0_01_en_CEK0803K.hdx%22,%22ICSLite_enterprise-en-22.2202.10.zip%22,%22Tele2%20EPC%20products.xlsx%22%5D[Ссылка для скачивания всех файлов]
+
. Выгрузить https://cloud.protei.ru/s/r8q4J2DOhy9yHj9/download?path=%2F&files=ICSLite_enterprise-en-22.2202.10.zip[архив] с программой 'ICS Lite' на свое устройство.
+
TIP: Имя архива -- `ICSLite_enterprise-en-22.2202.10.zip`.
+
. Распаковать загруженный архив.
. В распакованной директории запустить файл Startup.exe.
+
Файл запустит программное обеспечение, которое разворачивается на локальном хосте (127.0.0.1:51299).
+
.Главная страница ICS Lite
image::ics_lite_main_page.png[width=633]
+
. Перейти в директорию, куда был распакован архив.
. Перейти в директорию `libraries/`.
. Добавить в директорию файлы документации, загруженные из облака на шаге 1.
. Обновить главную страницу интерфейса ICS Lite, нажав кнопку btn:[Update] на верхней панели основного окна.
