= Работа с базами данных
:author: Andrew Tar <andrew.tar@yahoo.com>
:chapter-signifier:
:revnumber: 1.0.0
:experimental:
:icons: font

Необходимые данные для подключения к базе данных

Для подключения к базе данных на развернутом сервере в локальной сети требуются:

* Логин для авторизации;
* Пароль для авторизации;

== Подключение к базам данных MySQL

Алгоритм для создания соединения с базой данных:

. Перейти на локальную зону, где располагается база данных. +

TIP: Подробнее см. xref:putty.adoc#Connect[Работа с PuTTY].

[start=2]
. Смените текущего пользователя на superuser с помощью команды `su`.

[,console]
----
[support@192.168.100.254 ~] $ su
Password: <elephant>
[root@192.168.100.254 ~] #
----

TIP: Подробнее см. xref:cmd.adoc#su[Команда su].

[start=3]
. В строке консольного окна ввести команду `mysql` с логином для авторизации.

[,console]
----
[root@192.168.100.254 ~] # mysql -u admin
----

[start=4]
. В строке консольного окна ввести пароль для авторизации.

[,console]
----
Password: <sql>
----

== Дальнейшая работа с базой данных MySQL

Полное руководство см. https://dev.mysql.com/doc/refman/8.0/en/mysql-commands.html[Руководство MySQL].

== Базовые команды

=== Отобразить все базы данных

[source,sql]
----
SHOW databases;
----

.Пример
[source,sql]
----
mysql> SHOW databases;

+--------------------+
| Database           |
+--------------------+
| PROTEI_RG          |
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.001 sec)
----

NOTE: Базы `information_schema`, `mysql`, `performance_schema`, `sys` являются системными и ведутся СУБД автоматически.

=== Перейти к базе данных

[source,sql]
----
USE <database>;
----

.Пример
[source,sql]
----
mysql> USE PROTEI_RG;
MariaDB [Protei_RG]>
----

=== Отобразить все таблицы в базе данных

[source,sql]
----
SHOW tables;
----

.Пример
[source,sql]
----
MariaDB [Protei_RG]> SHOW tables;
+---------------------+
| Tables_in_Protei_RG |
+---------------------+
| Tm_DeleteImsiLog    |
| Tm_IMSI             |
| Tm_IMSI_old         |
| Tm_NumberList       |
| Tm_Templist         |
+---------------------+
5 rows in set (0.001 sec)
----

=== Отобразить все значения в таблице

[source,sql]
----
SELECT * FROM <table>;
----

.Пример
[source,sql]
----
MariaDB [Protei_RG]> SELECT * FROM Tm_IMSI;
+-----+---------------+-------------+-------+
| nID | nNumberListID | strIMSI     | dtExp |
+-----+---------------+-------------+-------+
| 1   | 100032        | 79231234123 | NULL  |
| 3   | 100130        | 79237654421 | NULL  |
+-----+---------------+-------------+-------+
2 rows in set (0.001 sec)
----

=== Отобразить описание параметров таблицы

[source,sql]
----
DESCRIBE <table>;
----

.Пример
[source,sql]
----
MariaDB [Protei_RG]> DESCRIBE Tm_IMSI;
+---------------+-------------+------+-----+---------+----------------+
| Field         | Type        | Null | Key | Default | Extra          |
+---------------+-------------+------+-----+---------+----------------+
| nID           | int(16)     | NO   | PRI | NULL    | auto_increment |
| nNumberListID | int(16)     | NO   | MUL | 0       |                |
| strIMSI       | varchar(15) | NO   | MUL | NULL    |                |
| dtExp         | timestamp   | YES  |     | NULL    |                |
+---------------+-------------+------+-----+---------+----------------+
4 rows in set (0.002 sec)
----

=== Выйти из базы данных

[source,sql]
----
exit;
----

.Пример
[source,sql]
----
mysql> exit;
Bye
----
