[[project-directories]]
= Директории проекта
:appendix-caption: Приложение
:appendix-refsig: Приложение
:caution-caption: Внимание
:chapter-signifier:
:example-caption: Пример
:experimental:
:figure-caption: Рисунок
:icons: font
:important-caption: Важно
:noheader:
:nofooter:
:note-caption: Примечание
:pagenums:
:partnums:
:pdf-fontsdir: ../../../fonts,GEM_FONTS_DIR
:pdf-page-layout: portrait
:pdf-theme: base-theme.yml
:pdf-themesdir: themes
:sectids:
:section-refsig:
:sectlinks:
:sectnumlevels: 5
:sectnums:
:showtitle:
:source-highlighter: rouge
:stylesdir: ../../../css
:stylesheet: default.css
:table-caption: Таблица
:text-align: left
:tip-caption: Совет
:title-page:
:toc-title: Содержание
:toc: auto
:toclevels: 5
:warning-caption: Предупреждение
:xrefstyle: full
:kroki-fetch-diagram:
:imagesdir: ../images
:toc:

[horizontal]
/usr/protei/%ProjectDir%/archive/:: директория для архивных файлов журналов
/usr/protei/backup/%ProjectDir%/:: директория для резервных копий с других узлов
/usr/protei/%ProjectDir%/bin/:: директория для исполняемых файлов
/usr/protei/%ProjectDir%/bin/utils/:: директория для запускаемых скриптов, реализующих основной функционал скриптовой оболочки
/usr/protei/%ProjectDir%/cdr/:: директория для журналов CDR
/usr/protei/%ProjectDir%/conf/:: директория для общих файлов настройки сервера Tomcat
/usr/protei/%ProjectDir%/config/:: директория для конфигурационных файлов
/usr/protei/%ProjectDir%/db/:: директория для конфигурации параметров базы данных
/usr/protei/%ProjectDir%/history/:: директория для архивных лог-файлов
/usr/protei/%ProjectDir%/jar/:: директория для архивированных файлов с классами Java
/usr/protei/%ProjectDir%/lib/:: директория для сторонних подключаемых библиотек
/usr/protei/%ProjectDir%/logs/:: директория для журналов
/usr/protei/%ProjectDir%/metrics/:: директория для файлов метрик
/usr/protei/%ProjectDir%/scripts/:: директория для хранения скриптов CLI
/usr/protei/%ProjectDir%/stat/:: директория для файлов статистики
/usr/protei/%ProjectDir%/temp/:: директория для временных файлов
/usr/protei/%ProjectDir%/webapps/:: директория для файлов Web-интерфейса