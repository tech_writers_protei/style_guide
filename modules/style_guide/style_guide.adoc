= Style Guide по оформлению документации
:asciidoctorconfigdir: .
:author: Andrew Tar
:appendix-caption: Приложение
:caution-caption: Внимание
:copyright: Без предварительного письменного разрешения, полученного от ООО "НТЦ ПРОТЕЙ", \
настоящий документ и любые выдержки из него, с изменениями и переводом на другие языки, \
не могут быть воспроизведены или использованы.
:docname: Style Guide
:description: Руководство по установившимся стандартам и договоренностям при оформлении \
документации для поддержания единого стиля.
:email: tarasov-a@protei.ru
:appendix-caption: Приложение
:appendix-refsig: Приложение
:author: Andrew Tar
:caution: Предупреждение
:chapter-signifier:
:email: andrew.tar@yahoo.com
:example-caption: Пример
:experimental:
:figure-caption: Рисунок
:icons: font
:important-caption: Важно
:noheader:
:nofooter:
:note-caption: Примечание
:pagenums:
:partnums:
:pdf-fontsdir: ../../fonts,GEM_FONTS_DIR
:pdf-page-layout: portrait
:pdf-theme: base-theme.yml
:pdf-themesdir: themes
:revnumber: 1.1.2
:sectids:
:section-refsig:
:sectlinks:
:sectnumlevels: 5
:sectnums:
:showtitle:
:source-highlighter: rouge
:stylesdir: ../../css
:stylesheet: default.css
:table-caption: Таблица
:text-align: left
:tip-caption: Совет
:title-page:
:toc-title: Содержание
:toc: auto
:toclevels: 5
:warning-caption: Предупреждение
:xrefstyle: full
:kroki-fetch-diagram:

ifndef::imagesdir[:imagesdir: images]

ifndef::directory[:directory: ..]

:leveloffset: +1

[preface]
include::{directory}/pages/introduction.adoc[Вступление]

<<<

include::{directory}/pages/principles.adoc[Основные принципы разработки Docs as Code]

<<<

include::{directory}/pages/scope.adoc[Область применения]

<<<

include::{directory}/pages/general_rules_md.adoc[Общие правила оформления]

<<<

include::{directory}/pages/checklist.adoc[Чек-лист]

<<<

include::{directory}/pages/reusing.adoc[Повторное использование файлов]

<<<

include::{directory}/pages/git.adoc[Работа с git]

<<<

include::{directory}/pages/settings.adoc[Используемые программы]

<<<

include::{directory}/pages/custom_software.adoc[Вспомогательные утилиты]
<<<

include::{directory}/pages/formatting.adoc[Форматирование текста]

<<<

include::{directory}/pages/code.adoc[Форматирование кода]

<<<

include::{directory}/pages/lists.adoc[Оформление списков]

<<<

include::{directory}/pages/image.adoc[Оформление изображений]

<<<

include::{directory}/pages/api_file.adoc[Оформление API]

<<<

include::{directory}/pages/cli_file.adoc[Оформление CLI]

// <<<
//
// include::{directory}/../how_to/pages/adding_links_md.adoc[Добавление ссылок в Markdown]
//
// <<<
//
// include::{directory}/../how_to/pages/adding_links_adoc.adoc[Добавление ссылок в AsciiDoc]

<<<

include::{directory}/pages/naming.adoc[Правила наименования]

<<<

include::{directory}/pages/directories.adoc[Директории проекта]

<<<

include::{directory}/pages/logs.adoc[Log-файлы]

// <<<
//
// include::{directory}/pages/pdf_generation.adoc[Конвертация документа в формат PDF]

<<<

include::{directory}/pages/data_types.adoc[Типы данных]

<<<

include::{directory}/pages/ompr.adoc[Обязательность и перегружаемость параметров]

<<<

include::{directory}/pages/config_file.adoc[Конфигурационные файлы]

<<<

include::{directory}/pages/project_structure.adoc[Структура]

<<<

include::{directory}/pages/template.adoc[Работа с шаблоном]

<<<

[appendix]
include::{directory}/pages/regexp.adoc[Регулярные выражения]

<<<

[appendix]
include::{directory}/pages/draw_io.adoc[Основы работы с draw.io]

<<<

[appendix]
include::{directory}/pages/cmd.adoc[Командная строка *nix]

<<<

[appendix]
include::{directory}/pages/html_tags.adoc[Использование HTML-тегов]

<<<

[appendix]
include::{directory}/pages/huawei_documentation.adoc[Документация Huawei]

<<<

[appendix]
include::{directory}/pages/placeholders.adoc[Подменные значения для примеров]

// <<<

// include::{directory}/pages/default_files.adoc[Файлы, используемые как есть]

// <<<

// include::{directory}/pages/main.adoc[]

// <<<

// include::{directory}/pages/notes.adoc[Оформление примечаний]

// <<<

// include::{directory}/pages/table.adoc[Оформление таблиц]

:leveloffset: -1
