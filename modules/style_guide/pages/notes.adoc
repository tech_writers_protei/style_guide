[[formatting-notes]]
= Оформление примечаний
:appendix-caption: Приложение
:appendix-refsig: Приложение
:caution-caption: Внимание
:chapter-signifier:
:example-caption: Пример
:experimental:
:figure-caption: Рисунок
:icons: font
:important-caption: Важно
:noheader:
:nofooter:
:note-caption: Примечание
:pagenums:
:partnums:
:pdf-fontsdir: ../../../fonts,GEM_FONTS_DIR
:pdf-page-layout: portrait
:pdf-theme: base-theme.yml
:pdf-themesdir: themes
:sectids:
:section-refsig:
:sectlinks:
:sectnumlevels: 5
:sectnums:
:showtitle:
:source-highlighter: rouge
:stylesdir: ../../../css
:stylesheet: default.css
:table-caption: Таблица
:text-align: left
:tip-caption: Совет
:title-page:
:toc-title: Содержание
:toc: auto
:toclevels: 5
:warning-caption: Предупреждение
:xrefstyle: full
:kroki-fetch-diagram:
:imagesdir: ../images
:toc: