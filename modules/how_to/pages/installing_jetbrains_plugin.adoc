[[plugin-jet-brains]]
= Как установить и настроить плагин AsciiDoc для PyCharm/IntelliJ?
:author: Andrew Tar
:appendix-caption: Приложение
:caution-caption: Внимание
:copyright: Без предварительного письменного разрешения, полученного от ООО "НТЦ ПРОТЕЙ", \
настоящий документ и любые выдержки из него, с изменениями и переводом на другие языки, \
не могут быть воспроизведены или использованы.
:email: tarasov-a@protei.ru
:example-caption: Пример
:experimental:
:figure-caption: Рисунок
:icons: font
:important-caption: Важно
:note-caption: Примечание
:pagenums:
:revdate: Mar 1, 2024
:revnumber: 1.1.1
:showtitle:
:sectnums:
:sectlinks:
:sectnumlevels: 5
:table-caption: Таблица
:text-align: left
:tip-caption: Совет
:toc: auto
:toc-title: Содержание
:toclevels: 5
:warning-caption: Предупреждение
:xrefstyle: full
:appendix-refsig: Приложение
:source-highlighter: rouge
:pdf-fontsdir: ../fonts,GEM_FONTS_DIR
:pdf-themesdir: ../themes
:pdf-theme: base-theme.yml
:stylesdir: ../css
:stylesheet: default.css
:nofooter:
:noheader:

ifndef::imagesdir[:imagesdir: ../images]

Для упрощения работы с файлами AsciiDoc можно использовать плагин.

Плагин разработан той же командой, что и язык с обработчиком и нативно поддерживает все возможности.

Он доступен для всех IDEA семейства JetBrains: IntelliJ, PyCharm, WriterSide, ...

Примеры приводятся в IDEA PyCharm.

.Немного информации о плагине
[%collapsible]
====
По факту, это реализация конвертера https://docs.asciidoctor.org/asciidoctorj/latest/[AsciidoctorJ].

Он является воплощением JRuby, обработкой Ruby с помощью Java в реальном времени.

Также имеет много различных
https://docs.asciidoctor.org/asciidoctorj/latest/extensions/extensions-introduction/[дополнений и доработок], помимо возможностей индивидуальных
https://intellij-asciidoc-plugin.ahus1.de/docs/users-guide/features/advanced/asciidoctor-extensions[улучшений].
====

// tag::install-plugin[]
== Установка плагина

=== Установка плагина в обычных условиях

. В верхнем меню перейти menu:PyCharm[Settings].
+
image::plugin_settings.png[title="Верхнее меню"]
+
. В боковом меню перейти на вкладку menu:Plugins[].
. В верхних вкладках выбрать menu:Marketplace[].
. В строке поиска ввести `asciidoc`.
. Выбрать плагин с названием `AsciiDoc`.
. В окне описания нажать кнопку btn:[Install].
+
image::install_plugin.png[title="Установка плагина",width=983,height=713]
+
. В нижнем меню нажать кнопку btn:[Apply].
+
image::apply_installation.png[title="Активировать плагин",width=983,height=713]

=== Установка плагина в случае запретов, санкций и т.п.

. Перейти в репозиторий GitHub разработчиков плагина, https://github.com/asciidoctor/asciidoctor-intellij-plugin[asciidoctor-intellij-plugin].
. Перейти в раздел с релизами, https://github.com/asciidoctor/asciidoctor-intellij-plugin/releases[Releases].
+
image::plugin_repo.png[title="Репозиторий разработчиков плагина"]
+
. В последнем релизе перейти к файлам релиза, Assets.
+
image::plugin_releases.png[title="Файлы релиза"]
+
. Скачать архив в формате zip, asciidoctor-intellij-plugin-<version>.zip.
+
image::download_plugin_zip.png[title="Загрузка архива плагина"]
+
. В верхнем меню перейти menu:PyCharm[Settings].
+
image::plugin_settings.png[title="Верхнее меню"]
+
. В боковом меню перейти на вкладку menu:Plugins[].
. В верхней панели выбрать image:cog.svg[width=20].
+
image::manage_plugins.png[title="Верхняя панель вкладки Pligins"]
+
. В выпадающем меню выбрать menu:Install from disk...[].
+
image::install_plugin_from_disk.png[title="Установка плагина с носителя"]
+
. Выбрать загруженный файл с плагином `asciidoctor-intellij-plugin`.
. В нижнем меню нажать кнопку btn:[Apply].
+
image::apply_installation.png[title="Активировать плагин",width=983,height=713]
// end::install-plugin[]

== Настройка плагина

Плагин можно настроить во вкладке menu:PyCharm[Settings] или с помощью файла `.editorconfig`.

Файл `.editorconfig` добавляется в корень проекта, вместе с файлами `.gitignore`, `.gitlab-ci.yml`.

Файл настройки можно найти link:../src/.editorconfig[здесь].

[source,editorconfig]
----
root = true

[*]
charset = utf-8
end_of_line = lf
indent_size = 2
indent_style = space
insert_final_newline = false
max_line_length = 120
tab_width = 2
trim_trailing_whitespace = true
ij_continuation_indent_size = 8
ij_formatter_off_tag = @formatter:off
ij_formatter_on_tag = @formatter:on
ij_formatter_tags_enabled = true
ij_smart_tabs = false
ij_visual_guides = none
ij_wrap_on_typing = false

[{*.ad,*.adoc,*.asciidoc,.asciidoctorconfig}]
ij_asciidoc_blank_lines_after_header = 1
ij_asciidoc_blank_lines_keep_after_header = 1
ij_asciidoc_formatting_enabled = true
ij_asciidoc_one_sentence_per_line = true

[{*.bash,*.sh,*.zsh}]
ij_shell_binary_ops_start_line = false
ij_shell_keep_column_alignment_padding = false
ij_shell_minify_program = false
ij_shell_redirect_followed_by_space = false
ij_shell_switch_cases_indented = true
ij_shell_use_unix_line_separator = true

[{*.htm,*.html,*.sht,*.shtm,*.shtml}]
ij_html_add_new_line_before_tags = body,div,p,form,h1,h2,h3
ij_html_align_attributes = true
ij_html_align_text = true
ij_html_attribute_wrap = normal
ij_html_block_comment_add_space = false
ij_html_block_comment_at_first_column = true
ij_html_do_not_align_children_of_min_lines = 0
ij_html_do_not_break_if_inline_tags = title,h1,h2,h3,h4,h5,h6,p
ij_html_do_not_indent_children_of_tags = html,body,thead,tbody,tfoot
ij_html_enforce_quotes = false
ij_html_inline_tags = a,abbr,acronym,b,basefont,bdo,big,br,cite,cite,code,dfn,em,font,i,img,input,kbd,label,q,s,samp,select,small,span,strike,strong,sub,sup,textarea,tt,u,var
ij_html_keep_blank_lines = 2
ij_html_keep_indents_on_empty_lines = false
ij_html_keep_line_breaks = true
ij_html_keep_line_breaks_in_text = true
ij_html_keep_whitespaces = false
ij_html_keep_whitespaces_inside = span,pre,textarea
ij_html_line_comment_at_first_column = true
ij_html_new_line_after_last_attribute = never
ij_html_new_line_before_first_attribute = never
ij_html_quote_style = double
ij_html_remove_new_line_before_tags = br
ij_html_space_after_tag_name = false
ij_html_space_around_equality_in_attribute = false
ij_html_space_inside_empty_tag = false
ij_html_text_wrap = normal

[{*.markdown,*.md}]
ij_continuation_indent_size = 4
ij_wrap_on_typing = true
ij_markdown_force_one_space_after_blockquote_symbol = true
ij_markdown_force_one_space_after_header_symbol = true
ij_markdown_force_one_space_after_list_bullet = true
ij_markdown_force_one_space_between_words = true
ij_markdown_insert_quote_arrows_on_wrap = true
ij_markdown_keep_indents_on_empty_lines = false
ij_markdown_keep_line_breaks_inside_text_blocks = true
ij_markdown_max_lines_around_block_elements = 1
ij_markdown_max_lines_around_header = 1
ij_markdown_max_lines_between_paragraphs = 1
ij_markdown_min_lines_around_block_elements = 1
ij_markdown_min_lines_around_header = 1
ij_markdown_min_lines_between_paragraphs = 1
ij_markdown_wrap_text_if_long = true
ij_markdown_wrap_text_inside_blockquotes = true

[{*.yaml,*.yml}]
ij_yaml_align_values_properties = do_not_align
ij_yaml_autoinsert_sequence_marker = true
ij_yaml_block_mapping_on_new_line = false
ij_yaml_indent_sequence_value = true
ij_yaml_keep_indents_on_empty_lines = false
ij_yaml_keep_line_breaks = true
ij_yaml_sequence_on_new_line = false
ij_yaml_space_before_colon = false
ij_yaml_spaces_within_braces = true
ij_yaml_spaces_within_brackets = true
----
