[[pdf-generation]]
= Как создать конфигурационный файл для конвертации в формат PDF?
:author: Andrew Tar
:email: tarasov-a@protei.ru
:example-caption: Пример
:experimental:
:figure-caption: Рисунок
:icons: font
:important-caption: Внимание
:note-caption: Примечание
:pagenums:
:revdate: Dec 6, 2023
:revnumber: 1.0.0
:showtitle:
:sectlinks:
:sectnumlevels: 5
:table-caption: Таблица
:text-align: left
:tip-caption: Совет
:toc: auto
:toc-title: Содержание
:toclevels: 5
:warning-caption: Предупреждение
:xrefstyle: full
:source-highlighter: rouge
// :asciidoctorconfigdir: ../
:spacex1: {nbsp}{nbsp}
:spacex2: {nbsp}{nbsp}{nbsp}{nbsp}

Для конвертации файла в формат PDF используется файл `PDF_Protei_<ProjectName>.yml`, лежащий в корне репозитория.

В нем указываются все файлы, которые должны присутствовать в PDF-файле, в нужном порядке.

IMPORTANT: Необходимо обновлять файл при изменении структуры проекта, т.е. при добавлении/удалении/перемещении/переименовании отображаемых директорий и файлов.

== Структура yml-файла

[source,yaml]
----
settings:
  title-page: "<ProjectName>"
  doctype: "book"
  toc: true
  figure-caption: "Рисунок"
  chapter-signifier: false
  toc-title: "Содержание"
  outlinelevels: 2
  title-logo-image: "images/logo.svg[top=4%,align=right,pdfwidth=3cm]"
  version: <VersionNumber>

Rights:
  title:
    value: "Юридическая информация"
    title-files: false
  index:
    - content/common/_index.md

<SectionName>:
  index:
    - <path_to_index_page>
  title:
    value: "<Title>"
    level: <Level>
  files:
    - <section_file_1.md>
    - <section_file_N.md>
----

.Пример файла
[%collapsible]
====
[source,yaml]
----
settings:
  title-page: "PROTEI MME"
  doctype: "book"
  toc: true
  figure-caption: "Рисунок"
  chapter-signifier: false
  toc-title: "Содержание"
  outlinelevels: 2
  title-logo-image: "images/logo.svg[top=4%,align=right,pdfwidth=3cm]"
  version: 1.47

Rights:
  title:
    value: "Юридическая информация"
    title-files: false
  index:
    - content/common/_index.md

Basics:
  index:
    - content/common/basics/general.md
  files:
    - content/common/basics/functionalities.md
    - content/common/basics/network_architecture.md
    - content/common/basics/internal_architecture.md
    - content/common/basics/requirements_hw_sw.md
    - content/common/basics/lifecycle.md
    - content/common/basics/specifications.md

OAM:
  title:
    value: "Эксплуатация, администрирование и обслуживание"
  files:
    - content/common/oam/installation.md
    - content/common/oam/system_management.md
    - content/common/oam/api.md
    - content/common/oam/cli.md
    - content/common/oam/configure_vrf.md
    - content/common/oam/add_imsi.md
    - content/common/oam/update.md
    - content/common/oam/backup_recovery.md
    - content/common/oam/performance_kpi.md

Config:
  index:
    - content/common/config/_index.md
  files:
    - content/common/config/ap.md
    - content/common/config/apn_rules.md
    - content/common/config/code_mapping_rules.md
    - content/common/config/diameter.md
    - content/common/config/diam_dest.md
    - content/common/config/dns.md
    - content/common/config/emergency_numbers.md
    - content/common/config/emergency_pdn.md
    - content/common/config/forbidden_imei_rules.md
    - content/common/config/gtp_c.md
    - content/common/config/http.md
    - content/common/config/imsi_rules.md
    - content/common/config/long_mnc.md
    - content/common/config/metrics.md
    - content/common/config/mme.md
    - content/common/config/pcap.md
    - content/common/config/qos_rules.md
    - content/common/config/rac_rules.md
    - content/common/config/s1ap.md
    - content/common/config/served_plmn.md
    - content/common/config/sgd.md
    - content/common/config/sgsap.md
    - content/common/config/tac_rules.md
    - content/common/config/trace.md
    - content/common/config/ue_trace.md
    - content/common/config/zc_rules.md

Config/Component:
  title:
    value: "Конфигурирование компонент"
    level: 3
  files:
    - content/common/config/component/config.md
    - content/common/config/component/diameter.md
    - content/common/config/component/gtp_c.md
    - content/common/config/component/s1ap.md
    - content/common/config/component/sgsap.md

Logging:
  index:
    - content/common/logging/_index.md

Logging/EDR:
  title:
    value: "EDR"
    level: 3
  files:
    - content/common/logging/edr/connect.md
    - content/common/logging/edr/dedicated_bearer.md
    - content/common/logging/edr/diam.md
    - content/common/logging/edr/enodeb.md
    - content/common/logging/edr/error_code.md
    - content/common/logging/edr/gtp_c.md
    - content/common/logging/edr/gtp_c_overload.md
    - content/common/logging/edr/http.md
    - content/common/logging/edr/irat_handover.md
    - content/common/logging/edr/lte_handover.md
    - content/common/logging/edr/paging.md
    - content/common/logging/edr/reject.md
    - content/common/logging/edr/s1ap.md
    - content/common/logging/edr/s1ap_context.md
    - content/common/logging/edr/s1ap_overload.md
    - content/common/logging/edr/sgsap.md
    - content/common/logging/edr/tau.md

Logging/stat:
  title:
    level: 3
  index:
    - content/common/logging/stat/_index.md
  files:
    - content/common/logging/stat/MME_Diameter.md
    - content/common/logging/stat/MME_handover.md
    - content/common/logging/stat/MME_paging.md
    - content/common/logging/stat/MME_resource.md
    - content/common/logging/stat/MME_s1_Attach.md
    - content/common/logging/stat/MME_s1_Detach.md
    - content/common/logging/stat/MME_s1_Bearer_Activation.md
    - content/common/logging/stat/MME_s1_Bearer_Deactivation.md
    - content/common/logging/stat/MME_s1_Bearer_Modification.md
    - content/common/logging/stat/MME_s1_Interface.md
    - content/common/logging/stat/MME_s1_Security.md
    - content/common/logging/stat/MME_s1_Service.md
    - content/common/logging/stat/MME_s11_Interface.md
    - content/common/logging/stat/MME_S6a_interface.md
    - content/common/logging/stat/MME_sgs_Interface.md
    - content/common/logging/stat/MME_sv_Interface.md
    - content/common/logging/stat/MME_tau.md
    - content/common/logging/stat/MME_users.md
----
====

<<<

== Описание секций

Астериском, `++*++`, отмечены параметры, которые не требуют изменений от пользователя, т.е. добавляются в каждый проект как есть.

[options="header",cols="3,8,1"]
|===
|Параметр |Описание |Тип

|settings |Параметры программы asciidoctor-pdf для конвертации в PDF. |object
|{ | |
|{spacex1}title-page *+(*)+* |Заголовок на титульной странице. |string
|{spacex1}doctype |Тип генерируемого документа. |string
|{spacex1}toc |Флаг создания содержания. |bool
|{spacex1}figure-caption |Текст, добавляемый к номеру рисунка. |string
|{spacex1}chapter-signifier |Флаг добавления текста к номеру главы. |bool
|{spacex1}toc-title |Заголовок содержания. |string
|{spacex1}outlinelevels |Количество уровней, добавляемых в содержание. |int
|{spacex1}title-logo-image| Изображение, добавляемое на титульную страницу. |string
|{spacex1}version *+(*)+* | Версия продукта. |string
|} | |
|Rights |Параметры отображения секции с юридической информацией. |object
|{ | |
|{spacex1}title |Параметры заголовка секции. |object
|{spacex1}{ | |
|{spacex2}value |Текст заголовка. |string
|{spacex2}title-files |Флаг отображения заголовка файла. |bool
|{spacex1}} | |
|{spacex1}index |Путь до файла для первой страницы. |[str]
|} | |
|<SectionName> *+(*)+* |Параметры отображения секции. |object
|{ | |
|{spacex1}title *+(*)+* |Параметры заголовка секции. |object
|{spacex1}{ | |
|{spacex2}value *+(*)+* |Текст заголовка. |string
|{spacex2}level *+(*)+* |Уровень заголовков страниц в секции. |bool
|{spacex2}title-files |Флаг отображения заголовка файла. |bool
|{spacex1}} | |
|{spacex1}index *+(*)+* |Перечень файлов, отображаемых на первой странице. |[str]
|{spacex1}files *+(*)+* |Файлы секции. |[str]
|} | |

|===

<<<

== Принципы создания файла

. В файле указываются только Markdown-файлы.
+
Изображения, вспомогательные файлы, скрипты и т.п. игнорируются.
+
. В файле указываются только непустые файлы.
+
Пустым считается файл, если в нем нет алфавитно-цифрового символов после блока с заголовками,
link:../sections_adoc/template#file-structure[front matter].
+
. Файлы указываются в том порядке, в котором должны располагаться в PDF.
+
Несмотря на возможные разбиения секций, файлы добавляются один за другим сверху вниз.
+
. Необходимо сохранять отступы и кавычки, где они присутствуют.
+
Формат YAML определяет структуру и вложенности, в том числе и с помощью отступов.
Также строки обязательно должны быть заключены в кавычки.
+
. Если указано, что строка переносится "как есть", то она переносится абсолютно в таком же виде, как приведено в примере или шаблоне.
+
Сборка PDF осуществляется не в репозитории проекта документации, а в репозитории отдела DevOps.
Поэтому некоторые параметры задаются не относительно текущего проекта, а специального проекта отдела DevOps.
Несмотря на возможные кажущиеся странности и некорректные значения необходимо использовать именно такие, поскольку именно так все будет работать как надо.

<<<

=== Алгоритм создания

. В начале файла указать параметры конвертации.
+
Все параметры, кроме `title-page` и `version`, остаются неизменными.
+
. Далее указать раздел с юридической информацией, выступающий в роли предисловия.
+
Раздел не изменен и переносится из проекта в проект как есть.
+
. Затем для каждого раздела последовательно указываются файлы, которые станут секциями одного уровня.

TIP: Рекомендуется называть раздел так же, как и путь до соответствующей директории в проекте, считая от `common/`.

.Зачем?
[%collapsible]
====
По факту, движку, обрабатывающему данный файл, абсолютно безразличны эти названия.
Поэтому называться они могут как угодно, лишь бы уникально.
Однако преимущества предложенного наименования:

* Легко найти директорию, которая преобразуется в раздел;
* Легко сверять, все ли нужные файлы указаны;
* Легко найти нужный раздел при добавлении или удалении файлов;
* Легко проверить правильность заданных уровней при наличии вложенных директорий.

Также при неудачной попытке сборки сотрудники DevOps чаще всего сами исправляют причину неуспеха выполнения pipeline.
Не сомневаюсь, что им будет легче это сделать, если будет явно прослеживаться взаимосвязь между структурой проекта и структурой yaml-файла.
====

* Заголовком раздела становится или заголовок файла, заданный в параметре `index`, или текст, заданный в параметре `title{two-colons}value`.
+
NOTE: Приоритет имеет параметр `title{two-colons}value`.
+
* Файлы, указанные в параметре `files`, становятся подразделами, т.е. на уровень ниже.
* Если задан параметр `title{two-colons}level`, то файл, указанный в параметре `index`, становится подразделом этого уровня.

.Подробнее
[%collapsible]
====
Пусть в файле имеется такая часть:

[source,yaml]
----
oam:
  title:
    value: "Эксплуатация, администрирование и обслуживание"
  files:
    - content/common/oam/installation.md
    - content/common/oam/system_management.md

oam/api:
  title:
    level: 3
  index:
    - content/common/oam/api/_index.md
  files:
    - content/common/oam/api/api_command.md
----

Тогда:

* Создается раздел *первого* уровня с названием `"Эксплуатация, администрирование и обслуживание"`.
* Создаются два раздела *второго* уровня с заголовками и текстом из файлов
`content/common/oam/installation.md` и `content/common/oam/system_management.md`.
* Создается еще один раздел *второго* уровня с заголовками и текстом из файла `content/common/oam/api/_index.md`.
* Создается раздел *третьего* уровня с заголовком и текстом из файла `content/common/oam/api/api_command.md`.
====

[[add-directory]]
=== Добавление директории в раздел

Чтобы добавить директорию `<directory>` с файлами в раздел `<base_section>` перед отдельными файлами, необходимо:

. Добавить директорию в виде нового раздела `<base_section>/<directory>` после необходимого файла в разделе `<base_section>`.
. Задать значения параметрам `title{two-colons}value`, `title{two-colons}level`, `index` и `files`.
. Добавить раздел `<base_section_N>` после раздела `<base_section>/<directory>`.
. Задать файлы в качестве значения параметра `files`.

.Пример
[%collapsible]
====
[source,yaml]
----
section_0:
  title:
    level: 3
    value: "Секция"
  files:
    - content/common/section/file_first.md
    - content/common/section/file_before.md

section_0/directory:
  title:
    level: 4
  index:
    - content/common/section_0/directory/_index.md
  files:
    - content/common/section_0/directory/directory_file.md

section_1:
  title:
    level: 3
  files:
    - content/common/section/file_after.md
    - content/common/section/file_last.md
----
====

<<<

== Шаблон для yaml-файла

Файл шаблона расположен link:../src/PDF_ProjectName.yml[здесь].

[source,yaml]
----
settings:
  title-page: "%ProjectName%"
  doctype: "book"
  toc: true
  figure-caption: "Рисунок"
  chapter-signifier: false
  toc-title: "Содержание"
  outlinelevels: 2
  title-logo-image: "images/logo.svg[top=4%,align=right,pdfwidth=3cm]"
  version: %VersionNumber%

Rights:
  title:
    value: "Юридическая информация"
    title-files: false
  index:
    - content/common/_index.md

#basics:
#  index:
#    - content/common/basics/general.md
#  files:
#    - content/common/basics/functionalities.md
#    - content/common/basics/licensing.md
#    - content/common/basics/network_architecture.md
#    - content/common/basics/internal_architecture.md
#    - content/common/basics/high_availability.md
#    - content/common/basics/requirements_hw_sw.md
#    - content/common/basics/lifecycle.md
#    - content/common/basics/scaling.md
#    - content/common/basics/roadmap.md
#    - content/common/basics/specifications.md

#description:
#  title:
#    value: "Дополнительная информация"
#  files:
#    - content/common/description/information_security.md
#    - content/common/description/access_policy.md
#    - content/common/description/storage_policy.md
#    - content/common/description/features.md
#    - content/common/description/call_flow.md

#components/%component%:
#  index:
#    - content/common/components/%component%/_index.md

#components/%component%/oam:
#  title:
#    value: "Эксплуатация, администрирование и обслуживание компоненты %component%"
#    level: 3
#  files:
#    - content/common/components/%component%/oam/installation.md
#    - content/common/components/%component%/oam/system_management.md
#    - content/common/components/%component%/oam/update.md
#    - content/common/components/%component%/oam/backup_recovery.md
#    - content/common/components/%component%/oam/performance_kpi.md

#components/%component%/config:
#  title:
#    level: 3
#  index:
#    - content/common/components/%component%/config/_index.md
#  files:
#    - %content/common/components/%component%/config/...%

#components/%component%/config/component:
#  title:
#    value: "Конфигурирование компонент"
#    level: 4
#  files:
#    - content/common/components/%component%/config/component/config.md
#    - %content/common/components/%component%/config/component/...%

#components/%component%/logging:
#  title:
#    level: 3
#  index:
#    - content/common/components/%component%/logging/_index.md

#components/%component%/logging/xdr:
#  title:
#    level: 4
#  index:
#    - content/common/components/%component%/logging/edr/_index.md
#  files:
#    - %content/common/components/%component%/logging/edr/...%

#components/%component%/logging/log:
#  title:
#    level: 4
#  index:
#    - content/common/components/%component%/logging/log/_index.md
#  files:
#    - %content/common/components/%component%/logging/log/...%

#components/%component%/logging/stat:
#  title:
#    level: 4
#  index:
#    - content/common/components/%component%/logging/stat/_index.md
#  files:
#    - %content/common/components/%component%/logging/stat/...%

#oam:
#  title:
#    value: "Эксплуатация, администрирование и обслуживание"
#  files:
#    - content/common/oam/installation.md
#    - content/common/oam/system_management.md

#oam/api:
#  title:
#    level: 3
#  index:
#    - content/common/oam/api/_index.md
#  files:
#    - %content/common/oam/api/...%

#oam/cli:
#  title:
#    level: 3
#  index:
#    - content/common/oam/cli/_index.md
#  files:
#    - %content/common/oam/cli/...%

#oam2:
#  files:
#    - content/common/oam/update.md
#    - content/common/oam/backup_recovery.md
#    - content/common/oam/performance_kpi.md

#config:
#  index:
#    - content/common/config/_index.md
#  files:
#    - %content/common/config/...%

#config/component:
#  title:
#    value: "Конфигурирование компонент"
#    level: 3
#  files:
#    - content/common/config/component/config.md
#    - %content/common/config/component/...%

#logging:
#  index:
#    - content/common/logging/_index.md

#logging/xdr:
#  title:
#    level: 3
#  index:
#    - content/common/logging/edr/_index.md
#  files:
#    - %content/common/logging/edr/...%

#logging/log:
#  title:
#    level: 3
#  index:
#    - content/common/logging/log/_index.md
#  files:
#    - %content/common/logging/log/...%

#logging/stat:
#  title:
#    level: 3
#  index:
#    - content/common/logging/stat/_index.md
#  files:
#    - %content/common/logging/stat/...%

#web:
#  index:
#    - content/common/web/_index.md
#  files:
#    - content/common/web/description.md
#    - content/common/web/config.md
#    - content/common/web/roles.md
#    - content/common/web/authorization.md
#    - content/common/web/main_page.md
#    - content/common/web/actions.md

#web/sections:
#  title:
#    level: 3
#    value: "Вкладки Web-интерфейса"
#  files:
#    - %content/common/web/sections/...%
----

<<<

=== Использование шаблона

. Заменить %ProjectName% в параметре `settings{two-colons}title-page`.
. Заменить %VersionNumber% в параметре `settings{two-colons}version`.
. Убрать знаки комментариев для файлов, которые используются в проекте и необходимы в PDF.
. В разделы `files` добавить соответствующие файлы проекта.
. При необходимости добавить новые разделы, не указанные в шаблоне, в требуемое место в файле согласно принципам, описанным ранее.

=== Алгоритм генерации PDF

. Создается пустой основной adoc-файл.
. В файл добавляются данные из файла PDF_*.yml.
. Определяются файлы, используемые для PDF.
. Каждый md-файл конвертируется в adoc-файл с помощью Pandoc.
. Содержимое каждого adoc-файла копируется в этот основной.
. Изменяются ссылки на файлы.
+
[%collapsible]
====
Ссылки сделаны для корректной презентации на сайте.
Однако с точки зрения логики, они неверные и ведут в никуда.
====
+
. Добавляются якоря для заголовков.
. Все картинки копируются в отдельную папку.
. Все ссылки на картинки в основном файле изменяются на ссылки на картинки в этой отдельной папке.
. Преобразуются уровни заголовков согласно заданным в файле PDF_*.yml.
. Добавляются новые заголовки, если такие есть, согласно заданным в файле PDF_*.yml.
. Выполняются некоторые манипуляции с текстом.
+
[%collapsible]
====
Pandoc очень универсален, и поэтому не все элементы распознает и преобразует правильно.
====
+
. Основной файл преобразуется в PDF с помощью библиотеки asciidoctor-pdf (разработана создателями языка AsciiDoc).