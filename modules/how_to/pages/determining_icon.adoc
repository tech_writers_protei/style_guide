= Как определить используемую иконку в Web-интерфейсе?
:figure-caption: Рисунок
:asciidoctorconfigdir: ../
:experimental:
:icons: font
:title: Как определить используемую иконку в Web-интерфейсе?
:showtitle:
:stylesdir: ../css
:stylesheet: default.css
:text-align: left
:tip-caption: Совет
:title-page:
:toc-title: Содержание
:toc: auto
:toclevels: 5

ifndef::imagesdir[:imagesdir: ../images]

Большинство иконок в Web-интерфейсах используют стандартные элементы, чаще всего Font Awesome.
Шрифт находится в открытом доступе, поэтому лучше добавить оригинальный файл, чем делать скриншот.

Чтобы определить нужную иконку, необходимо:

. Перейти на любую страницу Web-интерфейса, где находится иконка.
+
image::web_interface.png[title="Страница Web-интерфейса",width=960,align="center"]
+
. Навести курсор на иконку.
. ПКМ вызвать меню и выбрать Исследовать/Inspect.
+
image::inspect.png[title="Исследовать",align="center"]
+
. В открывшемся окне с HTML найти атрибут `class`.
+
image::icon_html.png[title="Структура HTML",width=960,align="center"]
+
. Найти значения, подобные `fa-*`/`glyphicon-*`/...
. Подстрока, следующая после дефиса, показывает название иконки в соответствующем наборе.
+
[NOTE]
--
Наиболее часто встречающиеся наборы:
[horizontal]
fa-*:: Font Awesome, https://fontawesome.com/
glyphicon-*:: Glyphicons, https://www.glyphicons.com/
icon-tabler-*:: Tabler, https://tabler.io/
--
+
. В свойствах элемента `This element` найти атрибут `color`/`text-color` и получить цвет в формате HEX.
+
NOTE: Для получения цвета в виде RGB, CMYK и др. необходимо нажать на кружок со цветом, зажав клавишу kbd:[Shift].