[[adding-links-adoc]]
= Как добавить ссылку в документ для AsciiDoc?
:appendix-caption: Приложение
:caution-caption: Внимание
:example-caption: Пример
:experimental:
:figure-caption: Рисунок
:icons: font
:important-caption: Важно
:note-caption: Примечание
:pagenums:
:showtitle:
:sectnums:
:sectlinks:
:sectnumlevels: 5
:table-caption: Таблица
:text-align: left
:tip-caption: Совет
:toc: auto
:toc-title: Содержание
:toclevels: 5
:warning-caption: Предупреждение
:xrefstyle: full
:appendix-refsig: Приложение
:source-highlighter: rouge
:pdf-fontsdir: ../fonts,GEM_FONTS_DIR
:pdf-themesdir: ../themes
:pdf-theme: base-theme.yml
:stylesdir: ../css
:stylesheet: default.css
:nofooter:
:noheader:

ifndef::imagesdir[]
:imagesdir: ../images/
endif::[]

[[adding-links-to-asciidoc]]
== Добавление ссылки на файл AsciiDoc

Операции со ссылкой на файл не всегда одинаковы и зависят от следующих параметров:

* Название файла, в который добавляется ссылка.
* Название файла, на который делается ссылка.

Точнее, не от названий файлов, а от того, есть ли среди них `index.adoc` или `index.adoc`.

Возможны 4 различных случая:

* <<adoc-adoc,В обычном AsciiDoc-файле на обычный AsciiDoc-файл>>;
* <<adoc-index,В обычном AsciiDoc-файле на AsciiDoc-файл `index.adoc` или `index.adoc`>>;
* <<index-adoc,В AsciiDoc-файле `index.adoc` или `index.adoc` на обычный AsciiDoc-файл>>;
* <<index-index-adoc,В AsciiDoc-файле `index.adoc` или `index.adoc` на AsciiDoc-файл `index.adoc` или `index.adoc`>>;

Подробный алгоритм см. <<algo-adoc,Пошаговый разбор алгоритма>>.

[[adoc-adoc]]
=== adoc-файл -> adoc-файл

Необходимо модифицировать обычную, корректную ссылку:

. Добавить `../` в начало ссылки.
. Удалить расширение `.adoc` в конце ссылки.
. Добавить `/` в конце ссылки.

[[adoc-index]]
=== adoc-файл -> index.adoc/_index.adoc

Необходимо модифицировать обычную, корректную ссылку:

. Добавить `../` в начало ссылки.
. Удалить `index.adoc` или `index.adoc` в конце ссылки.

[[index-adoc]]
=== index.adoc/_index.adoc -> adoc-файл

Необходимо модифицировать обычную, корректную ссылку:

. Удалить расширение `.adoc` в конце ссылки.
. Добавить `/` в конце ссылки.

[[index-index-adoc]]
=== index.adoc/_index.adoc -> index.adoc/_index.adoc

Необходимо модифицировать обычную, корректную ссылку:

. Удалить `index.adoc` или `index.adoc` в конце ссылки.

[[algo-adoc]]
== Пошаговый разбор алгоритма

Независимо от вышеуказанных параметров, вначале необходимо создать корректную ссылку.
Для определенности пусть необходимо добавить ссылки:

. [[link_1_adoc]]В файл *source.adoc* на файл *dest.adoc*;
. [[link_2_adoc]]В файл *source.adoc* на файл *_index.adoc* в *dir_dest*;
. [[link_3_adoc]]В файл *_index.adoc* в *dir_source* на файл *dest.adoc*;
. [[link_4_adoc]]В файл *_index.adoc* в *dir_source* на файл *_index.adoc* в *dir_dest*;

Структура файлов:

----
content/
  ├─common/
    ├─dir_source/
    | ├─_index.adoc
    | └─source.adoc
    |
    └─dir_dest/
      ├─_index.adoc
      └─dest.adoc
----

Сначала создаем обычные ссылки для перехода от одного файла к другому:

[source,text]
----
../dir_dest/dest.adoc <1>
../dir_dest/_index.adoc <2>
../dir_dest/dest.adoc <3>
../dir_dest/_index.adoc <4>
----

<1> <<link_1_adoc,ссылка 1>>
<2> <<link_2_adoc,ссылка 2>>
<3> <<link_3_adoc,ссылка 3>>
<4> <<link_4_adoc,ссылка 4>>

.Ссылка в обычном AsciiDoc-файле
....
Необходимо добавить ../ в начало пути.
....

.Ссылка в AsciiDoc-файле _index.adoc
....
Вносить никаких изменений не требуется.
....

[source,text]
----
../../dir_dest/dest.adoc <1>
../../dir_dest/_index.adoc <2>
../dir_dest/dest.adoc <3>
../dir_dest/_index.adoc <4>
----

<1> <<link_1_adoc,ссылка 1>>
<2> <<link_2_adoc,ссылка 2>>
<3> <<link_3_adoc,ссылка 3>>
<4> <<link_4_adoc,ссылка 4>>

.Ссылка на обычный AsciiDoc-файл
....
Необходимо заменить .adoc в конце пути на /.
....

.Ссылка на AsciiDoc-файл _index.adoc
....
Необходимо удалить _index.adoc в конце пути.
....

[source,text]
----
../../dir_dest/dest/ <1>
../../dir_dest/ <2>
../dir_dest/dest/ <3>
../dir_dest/ <4>
----

<1> <<link_1_adoc,ссылка 1>>
<2> <<link_2_adoc,ссылка 2>>
<3> <<link_3_adoc,ссылка 3>>
<4> <<link_4_adoc,ссылка 4>>

[IMPORTANT]
--
Если в ссылке имеется якорь, *#anchor*, то:

. Якорь временно отбрасывается.
. Выполняется порядок действий, описанный выше, в том числе и */*.
. Якорь возвращается в конец пути.
--

[source,console]
----
../../dir_dest/dest/#anchor
../../dir_dest/#anchor
../dir_dest/dest/#anchor
../dir_dest/#anchor
----

<<<

[[adding-links-to-not-asciidoc]]
== Добавление ссылки на файл не-AsciiDoc

Формат ссылки на файл зависит от названия файла, в который добавляется ссылка.

Независимо от вышеуказанных параметров, вначале необходимо создать корректную ссылку.
Для определенности пусть необходимо добавить ссылки:

. [[link_1_not_adoc]]В файл *source.adoc* на файл *DEST.svg* в *dir_dest*;
. [[link_2_not_adoc]]В файл *_index.adoc* в *dir_source* на файл *DEST.svg* в *dir_dest*;

NOTE: Имена файлов изображений, т.е. имеющих расширения *svg*, *png*, *jpg*, *jpeg*, *bmp* и др., записываются
*заглавными* *латинскими* буквами *без пробелов*.

Структура файлов:

----
content/
  └─common/
    ├─dir_source/
    | ├─_index.adoc
    | └─source.adoc
    |
    └─dir_dest/
      └─DEST.svg
----

Корректные ссылки:

[source,text]
----
../dir_dest/DEST.svg <1>
../dir_dest/DEST.svg <2>
----

<1> <<link_1_not_adoc,ссылка 1>>
<2> <<link_2_not_adoc,ссылка 2>>

.Ссылка в обычном AsciiDoc-файле
....
Необходимо добавить ../ в начало пути.
....

.Ссылка в AsciiDoc-файле _index.adoc
....
Вносить никаких изменений не требуется.
....

[source,text]
----
../dir_dest/DEST.svg <1>
../../dir_dest/DEST.svg <2>
----

<1> <<link_1_not_adoc,ссылка 1>>
<2> <<link_2_not_adoc,ссылка 2>>

.Почему так сложно?
[%collapsible]
====
Потому что фактически вот такая структура:

[source,text]
----
content/
  └─common/
    ├─dir_source_adoc/
    | ├─_index.adoc
    | └─source.adoc
    |
    ├─dir_dest_adoc/
    | ├─_index.adoc
    | └─dest.adoc
    |
    └─dir_images/
      └─IMAGE.svg
----

превращается в такую при создании сайта:

[source,text]
----
content/
  ├─common/
  | ├─dir_source_adoc/
  | | ├─index.html
  | | |
  | | └─source/
  | |   └─index.html
  | |
  | ├─dir_dest_adoc/
  | | ├─index.html
  | | |
  | | └─dest/
  | |   └─index.html
  | |
  | └─dir_images/
  |   └─IMAGE.svg
  |
  └─private/
----

То есть каждый AsciiDoc не index-файл превращается в директорию.

И по факту указываются ссылки не на файлы *.adoc.
Поэтому:

* Добавляется еще один уровень, поскольку исходный файл *source.adoc* становится *source/index.html*;
* Не указывается расширение **.adoc**, поскольку ссылка дается фактически на директорию.

Медиафайлы не превращаются в директории, как и файлы *index.adoc* и *_index.adoc*.
Однако они превращаются в файлы *index.html* и *_index.html*.
====

== Как создать корректную для сайта ссылку в IDE?

При работе в IDE можно несколько упростить создание ссылки.

Назовем ссылку работающей, если она содержит путь до существующей директории или файла.

В такой ссылке при наведении на любую часть пути с зажатой клавишей btn:[Ctrl] показывается, куда она указывает.

Если в ссылке указан путь до файла, то по нажатии на последнюю часть пути этот файл будет открыт.

image::ctrl_path_0.png[align="center",title="Интерактивный переход к директории ../../style_guide/"]

image::ctrl_path_1.png[align="center",title="Интерактивный переход к директории ../../style_guide/sample_directory/"]

image::ctrl_path_2.png[align="center",title="Интерактивный переход к файлу ../../style_guide/sample_directory/sample_file.adoc"]

.Алгоритм
. Создать работающую ссылку на файл, по которой можно перейти к файлу в самой IDE.
[numeric]
. Если файл назначения медийный, то перейти к следующему шагу. +
Если файл назначения *index.adoc* или *_index.adoc*, то удалить название файла. +
Если файл назначения обычный, то удалить расширение файла.
+
NOTE: При этом ссылка все еще остается работающей.
+
[numeric]
. Если исходный файл *index.adoc* или *_index.adoc*, то перейти к следующему шагу. +
Если исходный файл обычный, то добавить *../* в самом начале ссылки.
