[[adding-links-md]]
= Как добавить ссылку в документ для Markdown?
:appendix-caption: Приложение
:caution-caption: Внимание
:example-caption: Пример
:experimental:
:figure-caption: Рисунок
:icons: font
:important-caption: Важно
:note-caption: Примечание
:pagenums:
:showtitle:
:sectnums:
:sectlinks:
:sectnumlevels: 5
:table-caption: Таблица
:text-align: left
:tip-caption: Совет
:toc: auto
:toc-title: Содержание
:toclevels: 5
:warning-caption: Предупреждение
:xrefstyle: full
:appendix-refsig: Приложение
:source-highlighter: rouge
:pdf-fontsdir: ../fonts,GEM_FONTS_DIR
:pdf-themesdir: ../themes
:pdf-theme: base-theme.yml
:stylesdir: ../css
:stylesheet: default.css
:nofooter:
:noheader:

ifndef::imagesdir[]
:imagesdir: ../images/
endif::[]

[[adding-links-to-markdown]]
== Добавление ссылки на файл Markdown

Операции со ссылкой на файл не всегда одинаковы и зависят от следующих параметров:

* Название файла, в который добавляется ссылка.
* Название файла, на который делается ссылка.

Точнее, не от названий файлов, а от того, есть ли среди них `index.md` или `index.md`.

Возможны 4 различных случая:

* <<md-md,В обычном Markdown-файле на обычный Markdown-файл>>;
* <<md-index,В обычном Markdown-файле на Markdown-файл `index.md` или `index.md`>>;
* <<index-md,В Markdown-файле `index.md` или `index.md` на обычный Markdown-файл>>;
* <<index-index-md,В Markdown-файле `index.md` или `index.md` на Markdown-файл `index.md` или `index.md`>>;

Подробный алгоритм см. <<algo-md,Пошаговый разбор алгоритма>>.

[[md-md]]
=== Md-файл -> Md-файл

Необходимо модифицировать обычную, корректную ссылку:

. Добавить `../` в начало ссылки.
. Удалить расширение `.md` в конце ссылки.
. Добавить `/` в конце ссылки.

[[md-index]]
=== Md-файл -> index.md/_index.md

Необходимо модифицировать обычную, корректную ссылку:

. Добавить `../` в начало ссылки.
. Удалить `index.md` или `index.md` в конце ссылки.

[[index-md]]
=== index.md/_index.md -> Md-файл

Необходимо модифицировать обычную, корректную ссылку:

. Удалить расширение `.md` в конце ссылки.
. Добавить `/` в конце ссылки.

[[index-index-md]]
=== index.md/_index.md -> index.md/_index.md

Необходимо модифицировать обычную, корректную ссылку:

. Удалить `index.md` или `index.md` в конце ссылки.

[[algo-md]]
== Пошаговый разбор алгоритма

Независимо от вышеуказанных параметров, вначале необходимо создать корректную ссылку.
Для определенности пусть необходимо добавить ссылки:

. [[link_1_md]]В файл *source.md* на файл *dest.md*;
. [[link_2_md]]В файл *source.md* на файл *_index.md* в *dir_dest*;
. [[link_3_md]]В файл *_index.md* в *dir_source* на файл *dest.md*;
. [[link_4_md]]В файл *_index.md* в *dir_source* на файл *_index.md* в *dir_dest*;

Структура файлов:

----
content/
  └─common/
    ├─dir_source/
    | ├─_index.md
    | └─source.md
    |
    └─dir_dest/
      ├─_index.md
      └─dest.md
----

Сначала создаем обычные ссылки для перехода от одного файла к другому:

[source,text]
----
../dir_dest/dest.md <1>
../dir_dest/_index.md <2>
../dir_dest/dest.md <3>
../dir_dest/_index.md <4>
----

<1> <<link_1_md,ссылка 1>>
<2> <<link_2_md,ссылка 2>>
<3> <<link_3_md,ссылка 3>>
<4> <<link_4_md,ссылка 4>>

.Ссылка в обычном Markdown-файле
....
Необходимо добавить ../ в начало пути.
....

.Ссылка в Markdown-файле _index.md
....
Вносить никаких изменений не требуется.
....

[source,text]
----
../../dir_dest/dest.md <1>
../../dir_dest/_index.md <2>
../dir_dest/dest.md <3>
../dir_dest/_index.md <4>
----

<1> <<link_1_md,ссылка 1>>
<2> <<link_2_md,ссылка 2>>
<3> <<link_3_md,ссылка 3>>
<4> <<link_4_md,ссылка 4>>

.Ссылка на обычный Markdown-файл
....
Необходимо заменить .md в конце пути на /.
....

.Ссылка на Markdown-файл _index.md
....
Необходимо удалить _index.md в конце пути.
....

[source,text]
----
../../dir_dest/dest/ <1>
../../dir_dest/ <2>
../dir_dest/dest/ <3>
../dir_dest/ <4>
----

<1> <<link_1_md,ссылка 1>>
<2> <<link_2_md,ссылка 2>>
<3> <<link_3_md,ссылка 3>>
<4> <<link_4_md,ссылка 4>>

[IMPORTANT]
--
Если в ссылке имеется якорь, *#anchor*, то:

. Якорь временно отбрасывается.
. Выполняется порядок действий, описанный выше, в том числе и */*.
. Якорь возвращается в конец пути.
--

[source,text]
----
../../dir_dest/dest/#anchor/
../../dir_dest/#anchor/
../dir_dest/dest/#anchor/
../dir_dest/#anchor/
----

<<<

[[adding-links-to-not-markdown]]
== Добавление ссылки на файл не-Markdown

Формат ссылки на файл зависит от названия файла, в который добавляется ссылка.

Независимо от вышеуказанных параметров, вначале необходимо создать корректную ссылку.
Для определенности пусть необходимо добавить ссылки:

. [[link_1_not_md]]В файл *source.md* на файл *DEST.svg* в *dir_dest*;
. [[link_2_not_md]]В файл *_index.md* в *dir_source* на файл *DEST.svg* в *dir_dest*;

NOTE: Имена файлов изображений, т.е. имеющих расширения *svg*, *png*, *jpg*, *jpeg*, *bmp* и др., записываются
*заглавными* *латинскими* буквами *без пробелов*.

Структура файлов:

----
content/
  └─common/
    ├─dir_source/
    | ├─_index.md
    | └─source.md
    |
    └─dir_dest/
      └─DEST.svg
----

Корректные ссылки:

[source,text]
----
../dir_dest/DEST.svg <1>
../dir_dest/DEST.svg <2>
----

<1> <<link_1_not_md,ссылка 1>>
<2> <<link_2_not_md,ссылка 2>>

.Ссылка в обычном Markdown-файле
....
Необходимо добавить ../ в начало пути.
....

.Ссылка в Markdown-файле _index.md
....
Вносить никаких изменений не требуется.
....

[source,text]
----
../dir_dest/DEST.svg <1>
../../dir_dest/DEST.svg <2>
----

<1> <<link_1_not_md,ссылка 1>>
<2> <<link_2_not_md,ссылка 2>>

[[processing_hugo]]
== Почему так сложно?

Потому что фактически вот такая структура:

[source,text]
----
content/
  └─common/
    ├─dir_source_md/
    | ├─_index.md
    | └─source.md
    |
    ├─dir_dest_md/
    | ├─_index.md
    | └─dest.md
    |
    └─dir_images/
      └─IMAGE.svg
----

превращается в такую при создании сайта:

[source,text]
----
content/
  └─common/
    ├─dir_source_md/
    | ├─index.html
    | |
    | └─source/
    |   └─index.html
    |
    ├─dir_dest_md/
    | ├─index.html
    | |
    | └─dest/
    |   └─index.html
    |
    └─dir_images/
      └─IMAGE.svg
----

То есть каждый Markdown не index-файл превращается в директорию.

И по факту указываются ссылки не на файлы *.md.
Поэтому:

* Добавляется еще один уровень, поскольку исходный файл *source.md* становится *source/index.html*;
* Не указывается расширение **.md**, поскольку ссылка дается фактически на директорию.

Медиафайлы не превращаются в директории, как и файлы *index.md* и *_index.md*.
Однако они превращаются в файлы *index.html* и *_index.html*.

== Как создать корректную для сайта ссылку в IDE?

При работе в IDE можно несколько упростить создание ссылки.

Назовем ссылку работающей, если она содержит путь до существующей директории или файла.

В такой ссылке при наведении на любую часть пути с зажатой клавишей btn:[Ctrl] показывается, куда она указывает.

Если в ссылке указан путь до файла, то по нажатии на последнюю часть пути этот файл будет открыт.

image::ctrl_path_0.png[align="center",title="Интерактивный переход к директории ../../style_guide/"]

image::ctrl_path_1.png[align="center",title="Интерактивный переход к директории ../../style_guide/sample_directory/"]

image::ctrl_path_2.png[align="center",title="Интерактивный переход к файлу ../../style_guide/sample_directory/sample_file.md"]

.Алгоритм
. Создать работающую ссылку на файл, по которой можно перейти к файлу в самой IDE.
[numeric]
. Если файл назначения медийный, то перейти к следующему шагу. +
Если файл назначения *index.md* или *_index.md*, то удалить название файла. +
Если файл назначения обычный, то удалить расширение файла.
+
NOTE: При этом ссылка все еще остается работающей.
+
[numeric]
. Если исходный файл *index.md* или *_index.md*, то перейти к следующему шагу. +
Если исходный файл обычный, то добавить *../* в самом начале ссылки.