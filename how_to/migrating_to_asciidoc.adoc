[[migrating-to-asciidoc]]
= Как перейти с Markdown на AsciiDoc?
:doctype: book
:author: Andrew Tar
:appendix-caption: Приложение
:caution-caption: Внимание
:copyright: Без предварительного письменного разрешения, полученного от ООО "НТЦ ПРОТЕЙ", \
настоящий документ и любые выдержки из него, с изменениями и переводом на другие языки, \
не могут быть воспроизведены или использованы.
:docname: Миграция на AsciiDoc
:description: Описание смены файлов документации с формата *.md к формату *.adoc.
:email: tarasov-a@protei.ru
:example-caption: Пример
:experimental:
:figure-caption: Рисунок
:icons: font
:imagesdir: images
:important-caption: Важно
:note-caption: Примечание
:pagenums:
:revdate: Mar 5, 2024
:revnumber: 1.0.0
:showtitle:
:sectnums:
:sectlinks:
:sectnumlevels: 5
:table-caption: Таблица
:text-align: left
:tip-caption: Совет
:toc: auto
:toc-title: Содержание
:toclevels: 5
:warning-caption: Предупреждение
:xrefstyle: full
:appendix-refsig: Приложение
:source-highlighter: rouge
:pdf-fontsdir: fonts,GEM_FONTS_DIR
:pdf-themesdir: ./themes
:pdf-theme: base-theme.yml
:stylesdir: ./css
:stylesheet: default.css
:!partnums:
:chapter-label:
:asciidoctorconfigdir: ./

В документе приведены практические советы и примеры для перехода с Markdown на AsciiDoc, не привлекая внимание санитаров.

:leveloffset: +1

include::introduction.adoc[]

include::installing_jetbrains_plugin.adoc[]

include::ascii_doc_basics.adoc[]

:leveloffset: -1