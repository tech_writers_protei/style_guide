= Атрибуты документа
:page-aliases: document-attributes-reference.adoc
// TODO use icons or emoji for y and n
:y: Yes
:n: No
:endash: &#8211;
:url-epoch: https://reproducible-builds.org/specs/source-date-epoch/

Атрибуты документа используются для настройки работы обработчика или задания информации о документе и его окружении.
Здесь приведены все встроенные средства AsciiDoc.

Если не указано иное, атрибуты можно задать или убрать с помощью API, используя опцию `:attributes`, с помощью CLI, используя опцию `-a`, или в самом документе.

[horizontal]
Задан по умолчанию:: Атрибуту автоматически задается значение по умолчанию.
Это значение выделяется *полужирным*.

Значения:: Допустимые значения для атрибута.
Численные значения и значения, выделенные курсивом, показывают тип.
+
* _any_ -- Допускаются любые значения.
* _empty_ -- Атрибуту не требуется явное задание значения.
Атрибут уже активируется самим добавлением.
* _empty_[=`effective`] -- В некоторых случаях пустое значение интерпретируется обработчиком как одно из допустимых.
Это значение приводится в квадратных скобках со знаком равно, *=*.
* (`implied`) -- Встроенные не заданные атрибуты могут иметь встроенное значение.
Встроенное значение приводится в круглых скобках.
+
Если атрибут не принимает значения _any_ или _empty_, тогда можно задать значение только из перечня допустимых или обозначенного типа.

Только заголовок:: Атрибут должен быть задан только в заголовке документа.
Иначе, задание не будет иметь никакого эффекта.
Если атрибут не обозначен как _Только заголовок_, его можно задать в любом месте документа, при условии, что не заблокирован API или CLI.

== Intrinsic attributes

Intrinsic attributes are set automatically by the processor.
These attributes provide information about the document being processed (e.g., `docfile`), the security mode under which the processor is running (e.g., `safe-mode-name`), and information about the user's environment (e.g., `user-home`).

Many of these attributes are read only, which means they can't be modified (with some exceptions).
Attributes which are not are marked as modifiable.
Attributes marked as both modifiable and API/CLI Only can only be set from the API or CLI.
All other attributes marked as modifiable must be set by the end of the header (i.e., Header Only).

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Modifiable .>|API/CLI Only .>|Notes

|backend
|_any_ +
html5
|{y}
|{n}
|Используемый backend для выбора и активации конвертера для создания файла.
Как правило
Usually named according to the output format (e.g., `html5`)

|backend-<backend>
|_empty_
|{n}
|n/a
|A convenience attribute for checking which backend is selected.
<backend> is the value of the `backend` attribute (e.g., `backend-html5`).
Only one such attribute is set at time.

//|backend-<backend>-doctype-<doctype>
//|_empty_
//|{n}
//|n/a
//|A convenience attribute for checking the current backend/doctype combination.
//<backend> is the value of the `backend` attribute and <doctype> is the value of the `doctype` attribute (e.g., `backend-html5-doctype-article`).
//Only one such attribute is set at time.

|basebackend
|_any_ +
_пр._ html
|{n}
|n/a
|The generic backend on which the backend is based.
Typically derived from the backend value minus trailing numbers (e.g., the basebackend for `docbook5` is `docbook`).
May also indicate the internal backend employed by the converter (e.g., the basebackend for `pdf` is `html`).

|basebackend-<basebackend>
|_empty_
|{n}
|n/a
|A convenience attribute for checking which basebackend is active.
<basebackend> is the value of the `basebackend` attribute (e.g., `basebackend-html`).
Only one such attribute is set at time.

//|basebackend-<basebackend>-doctype-<doctype>
//|_empty_
//|{n}
//|n/a
//|A convenience attribute for checking the current basebackend/doctype combination.
//<basebackend> is the value of the `basebackend` attribute and <doctype> is the value of the `doctype` attribute (e.g., `basebackend-html-doctype-article`).
//Only one such attribute is set at time.

|docdate
|_date (ISO)_ +
_пр._ 2019-01-04
|{y}
|{n}
|Last modified date of the source document.^[<<note-docdatetime,1>>,<<note-sourcedateepoch,2>>]^

|docdatetime
|_datetime (ISO)_ +
_пр._ 2019-01-04 19:26:06 UTC
|{y}
|{n}
|Last modified date and time of the source document.^[<<note-docdatetime,1>>,<<note-sourcedateepoch,2>>]^

|docdir
|_directory path_ +
_пр._ /home/user/docs
|If input is a string
|{y}
|Full path of the directory that contains the source document.
Empty if the safe mode is SERVER or SECURE (to conceal the file's location).

|docfile
|_file path_ +
_пр._ /home/user/docs/userguide.adoc
|If input is a string
|{y}
|Full path of the source document.
Truncated to the basename if the safe mode is SERVER or SECURE (to conceal the file's location).

|docfilesuffix
|_file extension_ +
_пр._ .adoc
|If input is a string
|{y}
|File extension of the source document, including the leading period.

|docname
|_file stem basename_ +
_пр._ userguide
|If input is a string
|{y}
|Root name of the source document (no leading path or file extension).

|doctime
|_time (ISO)_ +
_пр._ 19:26:06 UTC
|{y}
|{n}
|Last modified time of the source document.^[<<note-docdatetime,1>>,<<note-sourcedateepoch,2>>]^

|doctype-<doctype>
|_empty_
|{n}
|n/a
|A convenience attribute for checking the doctype of the document.
<doctype> is the value of the `doctype` attribute (e.g., `doctype-book`).
Only one such attribute is set at time.

|docyear
|_integer_ +
_пр._ {docyear}
|{y}
|{n}
|Year that the document was last modified.^[<<note-docdatetime,1>>,<<note-sourcedateepoch,2>>]^

|embedded
|_empty_
|{n}
|n/a
|Only set if content is being converted to an embedded document (i.e., body of document only).

|filetype
|_any_ +
_пр._ html
|If input is a string
|{y}
|File extension of the output file name (without leading period).

|filetype-<filetype>
|_empty_
|{n}
|n/a
|A convenience attribute for checking the filetype of the output.
<filetype> is the value of the `filetype` attribute (e.g., `filetype-html`).
Only one such attribute is set at time.

|htmlsyntax
|`html` +
`xml`
|{n}
|n/a
|Syntax used when generating the HTML output.
Controlled by and derived from the backend name (html=html or xhtml=html).

|localdate
|_date (ISO)_ +
_пр._ 2019-02-17
|{y}
|{n}
|Date when the document was converted.^[<<note-sourcedateepoch,2>>]^

|localdatetime
|_datetime (ISO)_ +
_пр._ 2019-02-17 19:31:05 UTC
|{y}
|{n}
|Date and time when the document was converted.^[<<note-sourcedateepoch,2>>]^

|localtime
|_time (ISO)_ +
_пр._ 19:31:05 UTC
|{y}
|{n}
|Time when the document was converted.^[<<note-sourcedateepoch,2>>]^

|localyear
|_integer_ +
_пр._ {localyear}
|{y}
|{n}
|Year when the document was converted.^[<<note-sourcedateepoch,2>>]^

|outdir
|_directory path_ +
_пр._ /home/user/docs/dist
|{n}
|n/a
|Full path of the output directory.
(Cannot be referenced in the content.
Only available to the API once the document is converted).

|outfile
|_file path_ +
_пр._ /home/user/docs/dist/userguide.html
|{n}
|n/a
|Full path of the output file.
(Cannot be referenced in the content.
Only available to the API once the document is converted).

|outfilesuffix
|_file extension_ +
_пр._ .html
|{y}
|{n}
|File extension of the output file (starting with a period) as determined by the backend (`.html` for `html`, `.xml` for `docbook`, etc.).

|safe-mode-level
|`0` +
`1` +
`10` +
`20`
|{n}
|n/a
|Numeric value of the safe mode setting.
(0=UNSAFE, 1=SAFE, 10=SERVER, 20=SECURE).

|safe-mode-name
|`UNSAFE` +
`SAFE` +
`SERVER` +
`SECURE`
|{n}
|n/a
|Textual value of the safe mode setting.

|safe-mode-unsafe
|_empty_
|{n}
|n/a
|Set if the safe mode is UNSAFE.

|safe-mode-safe
|_empty_
|{n}
|n/a
|Set if the safe mode is SAFE.

|safe-mode-server
|_empty_
|{n}
|n/a
|Set if the safe mode is SERVER.

|safe-mode-secure
|_empty_
|{n}
|n/a
|Set if the safe mode is SECURE.

|user-home
|_directory path_ +
_пр._ /home/user
|{n}
|n/a
|Full path of the home directory for the current user.
Masked as `.` if the safe mode is SERVER or SECURE.
|===
[[note-docdatetime]]^[1]^ Only reflects the last modified time of the source document file.
It does not consider the last modified time of files which are included.

[[note-sourcedateepoch]]^[2]^ If the SOURCE_DATE_EPOCH environment variable is set, the value assigned to this attribute is built from a UTC date object that corresponds to the timestamp (as an integer) stored in that environment variable.
This override offers one way to make the conversion reproducible.
See the {url-epoch}[source date epoch specification] for more information about the SOURCE_DATE_EPOCH environment variable.
Otherwise, the date is expressed in the local time zone, which is reported as a time zone offset (e.g., `-0600`) or UTC if the time zone offset is 0).
To force the use of UTC, set the `TZ=UTC` environment variable when invoking Asciidoctor.

== Compliance attributes

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|Header Only .>|Notes

|attribute-missing
|`drop` +
`drop-line` +
`*skip*` +
`warn`
|{y}
|{n}
|Controls how xref:unresolved-references.adoc#missing[missing attribute references] are handled.

|attribute-undefined
|`drop` +
`*drop-line*`
|{y}
|{n}
|Controls how xref:unresolved-references.adoc#undefined[attribute unassignments] are handled.

|compat-mode
|_empty_
|{n}
|{n}
|Controls when the legacy parsing mode is used to parse the document.

|experimental
|_empty_
|{n}
|{y}
|Enables xref:macros:ui-macros.adoc[] and the xref:macros:keyboard-macro.adoc[].

|reproducible
|_empty_
|{n}
|{y}
|Prevents last-updated date from being added to HTML footer or DocBook info element.
Useful for storing the output in a source code control system as it prevents spurious changes every time you convert the document.
Alternately, you can use the SOURCE_DATE_EPOCH environment variable, which sets the epoch of all source documents and the local datetime to a fixed value.

|skip-front-matter
|_empty_
|{n}
|{y}
|Consume YAML-style frontmatter at top of document and store it in `front-matter` attribute.
//<<front-matter-added-for-static-site-generators>>
|===

[#builtin-attributes-i18n]
== Localization and numbering attributes

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|Header Only .>|Notes

|appendix-caption
|_any_ +
`*Appendix*`
|{y}
|{n}
|Метка, добавляемая к заголовку Приложения.

|appendix-number
|_character_ +
(`@`)
|{n}
|{n}
|Задает начальное значение для нумерации Приложения.

|appendix-refsig
|_any_ +
`*Appendix*`
|{y}
|{n}
|Signifier added to Appendix title cross references.

|caution-caption
|_any_ +
`*Caution*`
|{y}
|{n}
|Text used to label CAUTION admonitions when icons aren't enabled.

|chapter-number
|_number_ +
(`0`)
|{n}
|{n}
|Sets the seed value for the chapter number sequence.^[<<note-number,1>>]^
_Book doctype only_.

|chapter-refsig
|_any_ +
`*Chapter*`
|{y}
|{n}
|Signifier added to Chapter titles in cross references.
_Book doctype only_.

|chapter-signifier
|_any_
|{n}
|{n}
|xref:sections:chapters.adoc[Label added to level 1 section titles (chapters)].
_Book doctype only_.

|example-caption
|_any_ +
`*Example*`
|{y}
|{n}
|Text used to label example blocks.

|example-number
|_number_ +
(`0`)
|{n}
|{n}
|Sets the seed value for the example number sequence.^[<<note-number,1>>]^

|figure-caption
|_any_ +
`*Figure*`
|{y}
|{n}
|Text used to label images and figures.

|figure-number
|_number_ +
(`0`)
|{n}
|{n}
|Sets the seed value for the figure number sequence.^[<<note-number,1>>]^

|footnote-number
|_number_ +
(`0`)
|{n}
|{n}
|Sets the seed value for the footnote number sequence.^[<<note-number,1>>]^

|important-caption
|_any_ +
`*Important*`
|{y}
|{n}
|Text used to label IMPORTANT admonitions when icons are not enabled.

|lang
|_BCP 47 language tag_ +
(`en`)
|{n}
|{y}
|Language tag specified on document element of the output document.
Refer to https://html.spec.whatwg.org/#the-lang-and-xml:lang-attributes[the lang and xml:lang attributes section^] of the HTML specification to learn about the acceptable values for this attribute.

|last-update-label
|_any_ +
`*Last updated*`
|{y}
|{y}
|Text used for “Last updated” label in footer.

|listing-caption
|_any_
|{n}
|{n}
|Text used to label listing blocks.

|listing-number
|_number_ +
(`0`)
|{n}
|{n}
|Sets the seed value for the listing number sequence.^[<<note-number,1>>]^

|manname-title
|_any_ +
(`Name`)
|{n}
|{y}
|Label for program name section in the man page.

|nolang
|_empty_
|{n}
|{y}
|Prevents `lang` attribute from being added to root element of the output document.

|note-caption
|_any_ +
`*Note*`
|{y}
|{n}
|Text used to label NOTE admonitions when icons aren't enabled.

|part-refsig
|_any_ +
`*Part*`
|{y}
|{n}
|Signifier added to Part titles in cross references.
_Book doctype only_.

|part-signifier
|_any_
|{n}
|{n}
|xref:sections:chapters.adoc[Label added to level 0 section titles (parts)].
_Book doctype only_.

|preface-title
|_any_
|{n}
|{n}
|Title text for an anonymous preface when `doctype` is `book`.

|section-refsig
|_any_ +
`*Section*`
|{y}
|{n}
|Signifier added to title of numbered sections in cross reference text.

|table-caption
|_any_ +
`*Table*`
|{y}
|{n}
|Text of label prefixed to table titles.

|table-number
|_number_ +
(`0`)
|{n}
|{n}
|Sets the seed value for the table number sequence.^[<<note-number,1>>]^

|tip-caption
|_any_ +
`*Tip*`
|{y}
|{n}
|Text used to label TIP admonitions when icons aren't enabled.

|toc-title
|_any_ +
`*Table of Contents*`
|{y}
|{y}
|xref:toc:title.adoc[Title for table of contents].

|untitled-label
|_any_ +
`*Untitled*`
|{y}
|{y}
|Default document title if document doesn't have a document title.

|version-label
|_any_ +
`*Version*`
|{y}
|{y}
|See xref:document:version-label.adoc[].

|warning-caption
|_any_ +
`*Warning*`
|{y}
|{n}
|Text used to label WARNING admonitions when icons aren't enabled.
|===

== Document metadata attributes

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|Header Only .>|Notes

|app-name
|_any_
|{n}
|{y}
|Adds `application-name` meta element for mobile devices inside HTML document head.

|author
|_any_
|Extracted from author info line
|{y}
|Can be set automatically via the author info line or explicitly.
See xref:document:author-information.adoc[].

|authorinitials
|_any_
|Extracted from `author` attribute
|{y}
|Derived from the author attribute by default.
See xref:document:author-information.adoc[].

|authors
|_any_
|Extracted from author info line
|{y}
|Can be set automatically via the author info line or explicitly as a comma-separated value list.
See xref:document:author-information.adoc[].

|copyright
|_any_
|{n}
|{y}
|Adds `copyright` meta element in HTML document head.

|doctitle
|_any_
|Да, если у документа есть заголовок
|{y}
|Задает заголовок.

|description
|_any_
|{n}
|{y}
|Adds xref:document:metadata.adoc#description[description] meta element in HTML document head.

|email
|_any_
|Extracted from author info line
|{y}
|Can be any inline macro, such as a URL.
See xref:document:author-information.adoc[].

|firstname
|_any_
|Extracted from author info line
|{y}
|See xref:document:author-information.adoc[].

|front-matter
|_any_
|Yes, if front matter is captured
|n/a
|If `skip-front-matter` is set via the API or CLI, any YAML-style frontmatter skimmed from the top of the document is stored in this attribute.

|keywords
|_any_
|{n}
|{y}
|Adds xref:document:metadata.adoc#keywords[keywords] meta element in HTML document head.

|lastname
|_any_
|Extracted from author info line
|{y}
|See xref:document:author-information.adoc[].

|middlename
|_any_
|Extracted from author info line
|{y}
|See xref:document:author-information.adoc[].

|orgname
|_any_
|{n}
|{y}
|Adds `<orgname>` element value to DocBook info element.

|revdate
|_any_
|Extracted from revision info line
|{y}
|See xref:document:revision-information.adoc[].

|revremark
|_any_
|Extracted from revision info line
|{y}
|See xref:document:revision-information.adoc[].

|revnumber
|_any_
|Extracted from revision info line
|{y}
|See xref:document:revision-information.adoc[].

|title
|_any_
|{n}
|{y}
|Value of `<title>` element in HTML `<head>` or main DocBook `<info>` of output document.
Used as a fallback when the document title is not specified.
See xref:document:title.adoc#title-attr[title attribute].
|===

== Section title and table of contents attributes

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|Header Only .>|Notes

|idprefix
|_valid XML ID start character_ +
`*_*`
|{y}
|{n}
|Префикс автоматически создаваемых идентификаторов секций.

|idseparator
|_valid XML ID character_ +
`*_*`
|{y}
|{n}
|Word separator used in auto-generated section IDs.
See xref:sections:id-prefix-and-separator.adoc#separator[Change the ID word separator].

|leveloffset
|{startsb}+-{endsb}0{endash}5
|{n}
|{n}
|Изменяет уровень заголовков.
Знак + или --делает значение относительным.

|partnums
|_empty_
|{n}
|{n}
|Enables numbering of parts.
See xref:sections:part-numbers-and-labels.adoc#partnums[Number book parts].
_Book doctype only_.

|sectanchors
|_empty_
|{n}
|{n}
|xref:sections:title-links.adoc#anchor[Adds anchor in front of section title] on mouse cursor hover.

|sectids
|`empty`
|{y}
|{n}
|Generates and assigns an ID to any section that does not have an ID.
See xref:sections:auto-ids.adoc#disable[Disable automatic ID generation].

|sectlinks
|_empty_
|{n}
|{n}
|xref:sections:title-links.adoc[Turns section titles into self-referencing links].

|sectnums
|_empty_ +
`all`
|{n}
|{n}
|xref:sections:numbers.adoc[Numbers sections] to depth specified by `sectnumlevels`.

|sectnumlevels
|0{endash}5 +
(`3`)
|{n}
|{n}
|Задает уровень нумерации секций.

|title-separator
|_any_
|{n}
|{y}
|Символ для разделения заголовка и подзаголовка.

|toc
|_empty_[=`auto`] +
`auto` +
`left` +
`right` +
`macro` +
`preamble`
|{n}
|{y}
|Turns on xref:toc:indпр.adoc[table of contents] and specifies xref:toc:position.adoc[its location].

|toclevels
|1{endash}5 +
(`2`)
|{n}
|{y}
|Максимальный уровень секции.

|fragment
|_empty_
|{n}
|{y}
|Informs parser that document is a fragment and that it shouldn't enforce proper section nesting.
|===

== General content and formatting attributes

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|Header Only .>|Notes

|asset-uri-scheme
|_empty_ +
`http` +
(`https`)
|{n}
|{y}
|Controls protocol used for assets hosted on a CDN.

|cache-uri
|_empty_
|{n}
|{y}
|Cache content read from URIs.
//<<caching-uri-content>>

|data-uri
|_empty_
|{n}
|{y}
|Embed graphics as data-uri elements in HTML elements so file is completely self-contained.
//<<managing-images>>

|docinfo
|_empty_[=`private`] +
`shared` +
`private` +
`shared-head` +
`private-head` +
`shared-footer` +
`private-footer`
|{n}
|{y}
|Read input from one or more DocBook info files.
//<<naming-docinfo-files>>

|docinfodir
|_directory path_
|{n}
|{y}
|Расположение файлов docinfo.
По умолчанию совпадает с директорией исходного файла.

|docinfosubs
|_comma-separated substitution names_ +
(`attributes`)
|{n}
|{y}
|AsciiDoc substitutions that are applied to docinfo content.
//<<attribute-substitution-in-docinfo-files>>

|doctype
|`*article*` +
`book` +
`inline` +
`manpage`
|{y}
|{y}
|Тип итогового документа.

|eqnums
|_empty_[=`AMS`] +
`AMS` +
`all` +
`none`
|{n}
|{y}
|Controls automatic equation numbering on LaTeX blocks in HTML output (MathJax).
If the value is AMS, only LaTeX content enclosed in an `+\begin{equation}...\end{equation}+` container will be numbered.
If the value is all, then all LaTeX blocks will be numbered.
See https://docs.mathjax.org/en/v2.5-latest/tпр.html#automatic-equation-numbering[equation numbering in MathJax].

|hardbreaks-option
|_empty_
|{n}
|{n}
|xref:blocks:hard-line-breaks.adoc#per-document[Preserve hard line breaks].

|hide-uri-scheme
|_empty_
|{n}
|{n}
|xref:macros:links.adoc#hide-uri-scheme[Hides URI scheme] for raw links.

|media
|`prepress` +
`print` +
(`screen`)
|{n}
|{y}
|Specifies media type of output and enables behavior specific to that media type.
_PDF converter only_.

|nofooter
|_empty_
|{n}
|{y}
|Turns off footer.
//<<footer-docinfo-files>>

|nofootnotes
|_empty_
|{n}
|{y}
|Turns off footnotes.
//<<user-footnotes>>

|noheader
|_empty_
|{n}
|{y}
|Turns off header.
//<<doc-header>>

|notitle
|_empty_
|{n}
|{y}
|xref:document:title.adoc#hide-or-show[Hides the doctitle in an embedded document].
Mutually exclusive with the `showtitle` attribute.

|outfilesuffix
|`file extension` +
_пр._ .html
|{y}
|{y}
|File extension of output file, including dot (`.`), such as `.html`.
// <<navigating-between-source-files>>

|pagewidth
|_integer_ +
(`425`)
|{n}
|{y}
|Page width used to calculate the absolute width of tables in the DocBook output.

|relfileprefix
|_empty_ +
_path segment_
|{n}
|{n}
|The path prefix to add to relative xrefs.
//<<navigating-between-source-files>>

|relfilesuffix
|`расширение файла` +
_сегмент пути_ +
_пр._ .html
|{y}
|{n}
|The path suffix (e.g., file extension) to add to relative xrefs.
Defaults to the value of the `outfilesuffix` attribute.
(Preferred over modifying outfilesuffix).
//|<<navigating-between-source-files>>

|show-link-uri
|_empty_
|{n}
|{n}
|Добавляет URI ссылки после текста.
_Только для PDF_.

|showtitle
|_empty_
|{n}
|{y}
|Отображает заголовок в приложенном документе.
Не задается одновременно с атрибутом `notitle`.

|stem
|`_empty`[=`asciimath`] +
`asciimath` +
`latexmath`
|{n}
|{y}
|Активирует обработчик и интерпретатор математических выражений.

|table-frame
|(`all`) +
`ends` +
`sides` +
`none`
|{n}
|{n}
|Задает значение по умолчанию для атрибута `frame` в таблицах.

|table-grid
|(`all`) +
`cols` +
`rows` +
`none`
|{n}
|{n}
|Задает значение по умолчанию для атрибута `grid` в таблицах.

|table-stripes
|(`none`) +
`even` +
`odd` +
`hover` +
`all`
|{n}
|{n}
|Задает значение по умолчанию для атрибута `stripes` в таблицах.

|tabsize
|_integer_ (≥ 0)
|{n}
|{n}
|Конвертирует знаки табуляции в пробелы для блоков verbbatim.

|webfonts
|`empty`
|{y}
|{y}
|Control whether webfonts are loaded when using the default stylesheet.
When set to empty, uses the default font collection from Google Fonts.
A non-empty value replaces the `family` query string parameter in the Google Fonts URL.

|xrefstyle
|`full` +
`short` +
`basic`
|{n}
|{n}
|xref:macros:xref-text-and-style.adoc#cross-reference-styles[Formatting style to apply to cross reference text].
|===

== Image and icon attributes

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|Header Only .>|Notes

|iconfont-cdn
|_url_ +
(default CDN URL)
|{n}
|{y}
|Если не задано, используется cdnjs.com.
Переопределяет CDN, используемый для стиля Font Awesome.

|iconfont-name
|_any_ +
(`font-awesome`)
|{n}
|{y}
|Переопределяет название стиля иконок.
//<<icons>>

|iconfont-remote
|`empty`
|{y}
|{y}
|Allows use of a CDN for resolving the icon font.
Only relevant used when value of `icons` attribute is `font`.

|icons
|_empty_[=`image`] +
`image` +
`font`
|{n}
|{y}
|Chooses xref:macros:icons.adoc#icons-attribute[images or font icons] instead of text for admonitions.
Any other value is assumed to be an icontype and sets the value to empty (image-based icons).

|iconsdir
|_directory path_ +
_url_ +
_пр._ ./images/icons
|{y}
|{n}
|Location of non-font-based image icons.
Defaults to the _icons_ folder under `imagesdir` if `imagesdir` is specified and `iconsdir` is not specified.

|icontype
|`jpg` +
(`png`) +
`gif` +
`svg`
|{n}
|{n}
|File type for image icons.
Only relevant when using image-based icons.

|imagesdir
|`empty` +
_directory path_ +
_url_
|{y}
|{n}
|Location of image files.
|===

== Source highlighting and formatting attributes

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|Header Only .>|Notes

|coderay-css
|(`class`) +
`style`
|{n}
|{y}
|Controls whether CodeRay uses CSS classes or inline styles.

|coderay-linenums-mode
|`inline` +
(`table`)
|{n}
|{n}
|Sets how CodeRay inserts line numbers into source listings.

|coderay-unavailable
|_empty_
|{n}
|{y}
|Instructs processor not to load CodeRay.
Also set if processor fails to load CodeRay.

|highlightjsdir
|_directory path_ +
_url_ +
(default CDN URL)
|{n}
|{y}
|Location of the highlight.js source code highlighter library.

|highlightjs-theme
|_название темы highlight.js_ +
(`github`)
|{n}
|{y}
|Название темы, используемой highlight.js.

|prettifydir
|_directory path_ +
_url_ +
(default CDN URL)
|{n}
|{y}
|Location of non-CDN prettify source code highlighter library.

|prettify-theme
|_prettify style name_ +
(`prettify`)
|{n}
|{y}
|Name of theme used by prettify.

|prewrap
|`empty`
|{y}
|{n}
|xref:asciidoctor:html-backend:verbatim-line-wrap.adoc[Wrap wide code listings].

|pygments-css
|(`class`) +
`style`
|{n}
|{y}
|Controls whether Pygments uses CSS classes or inline styles.

|pygments-linenums-mode
|(`table`) +
`inline`
|{n}
|{n}
|Sets how Pygments inserts line numbers into source listings.

|pygments-style
|_Pygments style name_ +
(`default`)
|{n}
|{y}
|Name of style used by Pygments.

|pygments-unavailable
|_empty_
|{n}
|{y}
|Instructs processor not to load Pygments.
Also set if processor fails to load Pygments.

|rouge-css
|(`class`) +
`style`
|{n}
|{y}
|Controls whether Rouge uses CSS classes or inline styles.

|rouge-linenums-mode
|`inline` +
(`table`)
|{n}
|{n}
|Sets how Rouge inserts line numbers into source listings.
`inline` not yet supported by Asciidoctor.
See https://github.com/asciidoctor/asciidoctor/issues/3641[asciidoctor#3641].

|rouge-style
|_Rouge style name_ +
(`github`)
|{n}
|{y}
|Name of style used by Rouge.

|rouge-unavailable
|_empty_
|{n}
|{y}
|Instructs processor not to load Rouge.
Also set if processor fails to load Rouge.

|source-highlighter
|`coderay` +
`highlight.js` +
`pygments` +
`rouge`
|{n}
|{y}
|xref:verbatim:source-highlighter.adoc[Specifies source code highlighter].
Any other value is permitted, but must be supported by a custom syntax highlighter adapter.

|source-indent
|_integer_
|{n}
|{n}
|Normalize block indentation in source code listings.
//<<normalize-block-indentation>>

|source-language
|_source code language name_
|{n}
|{n}
|xref:verbatim:source-highlighter.adoc[Default language for source code blocks].

|source-linenums-option
|_empty_
|{n}
|{n}
|Turns on line numbers for source code listings.
|===

== HTML styling attributes

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|Header Only .>|Notes

|copycss
|`empty` +
_file path_
|{y}
|{y}
|Copy CSS files to output.
Only relevant when the `linkcss` attribute is set.
//<<applying-a-theme>>

|css-signature
|_valid XML ID_
|{n}
|{y}
|Assign value to `id` attribute of HTML `<body>` element.
*Preferred approach is to assign an ID to document title*.

|linkcss
|_empty_
|{n}
|{y}
|Links to stylesheet instead of embedding it.
Can't be unset in SECURE mode.
//<<styling-the-html-with-css>>

|max-width
|CSS length (e.g. 55em, 12cm, etc)
|{n}
|{y}
|Constrains maximum width of document body.
*Not recommended.
Use CSS stylesheet instead.*

|stylesdir
|_directory path_ +
_url_ +
`*.*`
|{y}
|{y}
|Location of CSS stylesheets.
//<<creating-a-theme>>

|stylesheet
|`empty` +
_file path_
|{y}
|{y}
|CSS stylesheet file name.
An empty value tells the converter to use the default stylesheet.
//<<applying-a-theme>>

|toc-class
|_valid CSS class name_ +
(`*toc*`) or (`*toc2*`) if toc=left
|{n}
|{y}
|CSS class on the table of contents container.
//<<user-toc>>
|===

== Manpage attributes

The attribute in this section are only relevant when using the manpage doctype and/or backend.

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|Header Only .>|Notes

|mantitle
|_any_
|Based on content.
|{y}
|Metadata for man page output.
//<<man-pages>>

|manvolnum
|_any_
|Based on content.
|{y}
|Metadata for man page output.
//<<man-pages>>

|manname
|_any_
|Based on content.
|{y}
|Metadata for man page output.
//<<man-pages>>

|manpurpose
|_any_
|Based on content
|{y}
|Metadata for man page output.
//<<man-pages>>

|man-linkstyle
|_link format pattern_ +
(`blue R <>`)
|{n}
|{y}
|Link style in man page output.
//<<man-pages>>

|mansource
|_any_
|{n}
|{y}
|Source (e.g., application and version) the man page describes.
//<<man-pages>>

|manmanual
|_any_
|{n}
|{y}
|Manual name displayed in the man page footer.
//<<man-pages>>
|===

== Security attributes

Since these attributes deal with security, they can only be set from the API or CLI.

[cols="30m,20,^10,^10,30"]
|===
.>|Name .>|Allowable Values .>|Set By Default .>|API/CLI Only .>|Notes

|allow-uri-read
|_empty_
|{n}
|{y}
|Allows data to be read from URLs.
//<<include-uri>>

|max-attribute-value-size
|_integer_ (≥ 0) +
`*4096*`
|If safe mode is SECURE
|{y}
|Limits maximum size (in bytes) of a resolved attribute value.
Default value is only set in SECURE mode.
Since attributes can reference attributes, it's possible to create an output document disproportionately larger than the input document without this limit in place.

|max-include-depth
|_integer_ (≥ 0) +
`*64*`
|{y}
|{y}
|Curtail infinite include loops and to limit the opportunity to exploit nested includes to compound the size of the output document.
//<<include-directive>>
|===

[[note-number]]^[1]^ The `-number` attributes are created and managed automatically by the AsciiDoc processor for numbered blocks.
They are only used if the corresponding `-caption` attribute is set (e.g., `listing-caption`) and the block has a title.
In Asciidoctor, setting the `-number` attributes will influence the next number used for subsequent numbered blocks of that type.
However, you should not rely on this behavior as it is subject to change in future revisions of the language.
// end::table[]
