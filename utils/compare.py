# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Iterable


def list_files(directory: str | Path) -> list[Path]:
    return sorted(p.name for p in Path(directory).iterdir())


def compare(dirs: Iterable[str | Path]):
    dict_files: dict[int, set[Path]] = {
        index: set(list_files(directory))
        for index, directory in enumerate(dirs)
    }

    files: set[Path] = set.union(*dict_files.values())

    print(files)

    for index, _ in enumerate(dirs):
        diff: set[Path] = files.difference(dict_files.get(index))
        diff_str: str = "\n".join(sorted(map(str, diff)))
        print(f"Not in {index} directory: {diff_str}")


if __name__ == '__main__':
    dir_1: str = "/Users/andrewtarasov/PycharmProjects/style_guide/sections_adoc"
    dir_2: str = "/Users/andrewtarasov/PycharmProjects/style_guide/modules/style_guide/pages"
    dirs: tuple[str, ...] = (dir_1, dir_2)

    compare(dirs)
